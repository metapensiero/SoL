# -*- coding: utf-8 -*-
# :Project:   SoL — Nix flake
# :Created:   sab 7 ott 2023, 12:06:15
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2023, 2024, 2025 Lele Gaifax
#

{
  description = "SoL development environment";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    gitignore = {
      url = "github:hercules-ci/gitignore.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      gitignore,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs { inherit system; };
        sol = import ./release.nix {
          inherit pkgs;
          inherit (gitignore.lib) gitignoreFilterWith;
        };
        sol-dev = sol.overridePythonAttrs { preBuild = ""; };
        python = pkgs.python3.withPackages (
          ps: [ sol-dev ] ++ sol-dev.optional-dependencies.dev ++ sol-dev.optional-dependencies.test
        );
        inherit (pkgs) glibcLocales;
        inherit (pkgs.lib) optionalString;
        inherit (pkgs.stdenv) isLinux;
      in
      {
        packages = {
          inherit sol;
          default = sol;
        };

        devShell = pkgs.mkShell {
          name = "SoL-dev";

          packages = [
            pkgs.gettext
            pkgs.gnumake
            python
          ];

          # Fix "locale.Error: unsupported locale setting" on non-NixOS,
          # see https://nixos.wiki/wiki/Locales
          LOCALE_ARCHIVE = optionalString isLinux "${glibcLocales}/lib/locale/locale-archive";

          # Let ReportLab know where Dejavu fonts are
          XDG_DATA_HOME = "${pkgs.dejavu_fonts}/share";

          shellHook = ''
            export PYTHONPATH="$PWD"/src''${PYTHONPATH:+:}$PYTHONPATH
          '';
        };
      }
    );
}
