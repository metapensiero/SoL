.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mar 07 apr 2009 14:13:48 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2009, 2010, 2014 Lele Gaifax
..

==============================
 International Laws of Carrom
==============================

This is a reformatted version of
http://www.carrom.org/icf/?page=1&subcat=20

.. contents:: Contents
   :local:

Interpretations
===============

In these Laws, unless there is anything repugnant to the subject or
context, the terms given below shall have the following
interpretations:

.. glossary::

   Laws
     shall mean the Laws of Carrom.

   Proper
     shall mean in accordance with the Laws.

   Improper
     shall mean contrary to the Laws.

   C/B
     shall mean Carrom Board.

   C/m
     shall mean Carromman / Carrommen.

   Board
     shall mean break to completion.

   Break
     shall mean the first stroke of a board.

   Finish
     shall mean the completion of the board.

   Player
     shall mean a Carrom player.

   Placing
     shall mean keeping the penalty and/or Due C/m in a flat position
     within the Outer circle only by the player permitted to do so as per
     the Laws. The Queen and/or the jumped C/m shall always be placed by
     the Umpire in the Centre Circle.

   Pocketing
     shall mean putting C/m and or Queen in the pocket by a proper /
     improper stroke.

   Push
     shall mean a jerk or sudden motion of the elbow of the playing
     "hand" instead of striking the Striker with the tip of the finger.

   Queen
     shall mean the red C/m.

   Due
     shall mean pocketing the Striker with or without C/m.

   Penalty
     shall mean punishment for infringement or violation of the Laws.

   Covering
     shall mean pocketing one of his own C/m by a player in the same or immediate subsequent stroke, when or after the Queen is pocketed.

   Shot
     shall mean a pair or cannon.

   Pair
     shall mean placing a C/m withing the Outer Circle near another C/m
     and/or Queen in such a way the there is space and the existing C/m
     faces the general direction of a pocket.

   Cannon
     shall mean placing a C/m within the Outer Circle near another C/m
     and/or Queen in such a way that there is no space and the existing
     C/m faces the general direction of a pocket.

   Thumbing
     shall mean taking a stroke with the thumb.

   Turn
     shall mean the right to strike.

   Chief Referee
     shall mean an official appointed to supervise and/or control a match.

   Umpire
     shall mean an official appointed to supervise and/or control a match.

   Opponent in Singles
     shall mean the player presently not having his turn of play.

   Opponent in Doubles
     shall mean the player sitting on the left and/or right side of the
     player presently having his turn to play.

   Hand
     shall mean the portion of the playing hand from the fingers up to the wrist.

   Finger
     shall mean the portion of the nailside of a finger up to the second joint.

   Imaginary lines
     shall mean the lines drawn in extension of the arrows between the Base Circles.

   Stroke
     shall mean hitting the C/m by the Striker directly or indirectly.

   White Slam
     shall mean pocketing all nine white C/m and the Queen as per Laws in
     the first turn of play. It may also be called "Break to Finish".

   Black Slam
     shall mean pocketing all the remaining black C/m with or without
     Queen as per Laws in the first turn of play.

.. note:: Words implying singular shall include plural and words implying
          masculine shall refer to the feminine also.

Sitting Position
================

1. In Singles, the players shall sit opposite to each other.

2. In Doubles, the partners shall sit opposite to each other,
   occupying all four sides.

3. Position adopted for sitting by a player before taking his turn to
   strike may be changed at any time provided the chair or stool on
   which he is sitting is not lifted, moved and/or disturbed during
   his turn of play.

4.

  (a) During the board no part of the body of a player, except the
      playing arm shall touch the Carrom Board, stand or table on
      which the C/B is placed.

  (b) However, wearings/clothing, ring, bangles, and/or watch worn by
      a player are exempted during his turn of play, but these should
      not touch the playing surface.

5. Use of any material to raise and/or adjust the height of the seat
   is permissible after the completion of the board only.

6. No part of the body, except the "hand" of the player shall go
   beyond the imaginary lines of the arrows.

How to Strike
=============

7. The Striker shall be struck and not pushed.

8. The Stroke shall be made with the finger with or without support of
   other fingers.

9. Any hand may be used in play.

10. While taking the stroke, the "hand" may touch the playing surface.

11.

   (a) The elbow of the playing "hand" shall not come within the
       playing surface nor shall extend beyond the imaginary lines of
       the arrows.

   (b) The "hand" may, however, cross the arrow.

12.

   (a) While making a stroke, taking support of the Stool or Chair,
       Stand or Table of the C/B and/or keeping the legs on the rim of
       the stand/table, by the player, is not permissible.

   (b) However, hands may rest on his body and legs, may rest on the
       rim of the stool or chair on which he sits.

Toss
====

13.

   (a) There shall be a toss by the Umpire at the commencement of each
       match. The toss shall be by spin of coin or by means of calling
       the C/m. The player/pair winning the toss shall have the choice
       of side or the option to strike first. Should the winner decide
       to have the choice of side, he shall indicate the same to the
       Umpire, who shall instruct the loser to sit first.

   (b) In Doubles, the pair winning the toss shall have the option as
       above.

   (c) If, however, break is chosen by the winner of the toss, the
       choice of side shall lie with the losers, and the winner shall
       have to sit first.

   (d) Once the losers have sat down, they cannot interchange. This
       order of sitting shall continue throughout the match.

Trial Board
===========

There shall be two trial boards only after the toss and before the
start of the match, one for each player or pair.

Break
=====

14.

   (a) Before the break, the C/m are so arranged, in a flat position,
       that the Queen shall occupy the Centre Circle and the rest of
       the C/m are placed around the Queen in the first row keeping the
       Black and White C/m alternately. In the second row, three white
       C/m will form the shape "Y" with the while C/m in the first
       row. The remaining space is filled up by placing black and white
       C/m alternately. All C/m so arranged, should be in compact
       round, touching each other, within the Outer Circle. A player
       may use his fingers or striker to keep the C/m intact.

   (b) The C/m should be arranged for the break with least possible
       loss of time after each board.

15. Break is taken by a player who has chosen to strike first.

16. The player who is to break shall have the white C/m during that
    board leaving the black C/m to his opponent. The Queen shall be the
    common C/m.

17. Break is considered to have been made if the Striker touches any of
    the C/m even slightly.

18.

   (a) Break is not considered to have been made if no C/m is touched
       by the Striker in the usual run or jumping out. In that case, a
       maximum of two more chances shall be allowed.

   (b) If after permissible number of chances, no C/m is touched, the
       right to break shall be lost and the turn to play shall pass to
       the opponent who shall have black C/m for play but no
       re-arrangement of C/m already arranged, shall be permitted. The
       above condition will prevail till the break is effected.

   (c) If a player in his attempt to break, plays an improper stroke or
       pockets his Striker without touching any of the C/m, he shall
       lose his turn. However, Due/penalty shall not be applicable.

19. The break shall be taken only after the Umpire calls "Play" and the
    stroke shall be made within 15 seconds of such call. The play shall
    be deemed to have begun from the moment the Umpire calls "Play."

20. If the break is made before the Umpire calls "Play", all the C/m
    and/or Queen pocketed shall be taken out for placing and an
    additional C/m as penalty. Due shall be declared, The player shall
    lose his turn.

Turn of Play
============

21. As long as a player pockets his own C/m and/or Queen in accordance
    with the Laws, his turn shall continue. Otherwise it shall pass on
    to the opponent.

22. Turns:

    (a) Singles

        i. In the first game, the player who chooses to break the first
           board shall have the white C/m. The turn to break shall pass
           alternately during the game.

        ii. In the second game the player who did not have the first
            turn to break shall have his turn first.

        iii. In the third game, the turn to break passes on to the
             first player.

    (b) In Doubles, however, the turn passes on to the player sitting
        to the right hand side of the player who had his turn.

23. A player shall not take more than 15 seconds for making a stroke
    from the moment the C/m/Queen/Striker has come to rest and picked
    up by the opponent and/or after observing the time limit for
    placing the C/m and/or Queen and/or forgoing Due/penalty C/m, if
    any.


24. If the turn is availed by a player out of turn, before the Umpire
    could control, the offending player shall lose the board by the
    number of C/m and Queen as are on the C/B. If the above remains
    unnoticed, till the next stroke is taken, the turn shall be allowed
    and the next turn shall be as per the Laws.

How to Score
============

25.

   (a) The player who completes pocketing all his C/m first wins the
       board.

   (b) The value/points are as follows

       i. Queen: 3 points up to and including 21 points.

       ii. C/m: 1 point each.

26.

   (a) The number of C/m of the opponent on the C/B shall be the points
       gained by that player in that board.

   (b) The player is entitled to be credited with the value of the
       Queen, only if he wins the board.

   (c) The player who loses the board is not credited with the value of
       the Queen, even if he has pocketed and covered the Queen.

27. The player loses the advantage of getting the credit of an
    additional 3 points for covering the Queen, once he has reached the
    score of 22 points.

28. The maximum number of points that can be scored in a board is 12
    only. Any Due and/or penalty C/m shall automatically be written
    off.

29.

   (a) A game shall be of 25 points or eight boards. The player who
       reaches 25 points first or leads at the conclusion of the eighth
       board shall be the winner of the game.

   (b) Up to and including the prequarter final rounds, each game shall
       be decided on the basis of eight boards. In case the score is
       equal at the end of the eighth board, an extra board shall be
       played to decide the winner. Before the extra board, there shall
       be a toss to choose break only.

30. All matches shall be decided by the best of three games only.

Change of Sides
===============

31. In Singles, the change of sides by the players shall be made in the
    opposite direction at the conclusion of each game.

32. In Doubles, the change of sides by the players shall be made to the
    next right hand side, at the conclusion of each game.

33.

   (a) In the third game for matches up to and including prequarter
       finals, the change of sides shall be made after the fourth board
       or after any player/pair has reached 13 points, whichever is
       earlier.

   (b) From Quarter Finals onwards the change of sides shall be made
       only after 13 points have been scored by any player/pair.

   (c) The change of sides, remaining unnoticed by the Umpire or any
       player, shall take place as and when noticed, but after
       completion of that particular board.

34. The players shall not take more than two minutes to change sides.

Fouls
=====

35. In general, any act of violation of Laws or anthing done contrary
    to what is stated specifically or implied (as undersood commonly)
    in these Laws, shall be broadly classified as:

    (a) Technical Foul
    (b) Foul

Technical Foul
--------------

36.

   (a) Any act of violation of the Laws committed by the player before
       the first stroke of his turn shall be deemed as a Technical
       Foul. A Technical Foul shall entail one C/m of the offending
       player being brought out for placing by the opponent and his
       turn shall continue.

   (b) Any act of violation of the Laws committed by the player
       presently not having his turn shall also be deemed as a
       Technical Foul. A penalty shall be imposed as per Rule 63(a).

Foul
----

37.

   (a) Any act of violation of the Laws committed by the player during
       or after the first stroke of his turn shall be deemed as a
       Foul. A foul shall entail one C/m of the offending player being
       brought out for placing by the opponent and the turn to play
       shall be lost.

   (b) While pocketing the C/m and/or Queen, if a foul is committed,
       the number of C/m and/or Queen so pocketed shall be brought out
       for placing and the turn of the player shall be lost.

C/m Overboard
=============

38.

   (a) If a C/m and/or Queen jump out of the playing surface, the
       jumped C/m and/or Queen shall be placed by the Umpire in the
       Centre Circle, if space permits, covering it fully or the
       maximum portion of it that is available.

   (b) If both the Queen and a C/m jump in the same stroke, preference
       shall be given to place the Queen first, and the jumped C/m
       shall be placed, touching the Queen, in the opposite direction
       of the player presently having his turn.

   (c) If both White and Black C/m jump in the same stroke, preference
       shall be given to place the C/m of the player who made that
       stroke first and the other C/m shall be placed, touching the
       first C/m in the manner described in the previous point.

   (d) If more than 2 C/m jump in the same stroke, the placing of the
       first 2 C/m shall be in accordance with previous points. The
       rest of the C/m shall be placed touching the first 2 C/m, as far
       as possible.

39.

   (a) If C/m and/or Queen jump out and fall back on the playing
       surface, the C/m and/or Queen shall be placed by the Umpire in
       the Centre Cirlce as per the Laws. The position of the disturbed
       C/m, if any, shall be corrected by the Umpire, as fas as
       possible, at his discretion.

   (b) If, however, C/m and.or Queen jump out and fall back on the
       playing surface after hitting the shade, bulb or light fittings,
       it shall be considered to have naturally travelled. Disturbed
       C/m, if any, shall not be rearranged.

C/m Rolling and Overlapping
===========================

40. If C/m and/or Queen stands up on its rim, it shall be allowed to
    remain as it is.

41. If two C/m and/or Queen overlap each other, they shall be left
    undisturbed.

42.

   (a) If the Striker rests on C/m and/or Queen, the Striker shall be
       removed without disturbance to the C/m and/or Queen by the
       Umpire. If disturbed, the original position of the C/m and/or
       Queen shall be restored, as fas as possible, by the Umpire.

   (b) If this happens at the mouth of the pocket and in the process of
       removal of the Striker the C/m and/or Queen lose its centre of
       gravity and fall into the pocket, they shall be deemed to have
       been pocketed.

43.

   (a) If C/m and/or Queen rest on the Striker, the Striker shall be
       removed by the Umpire by lifting the C/m and/or Queen and
       replacing them, as far as possible, in the position where they
       would rest if the Striker was not there.

   (b) If, however, this happens at the mouth of the pocket and in the
       process of removal of the C/m and/or Queen, the Striker lose its
       centre of gravity and falls into the pocket, it shall be deemed
       to have been pocketed. A penalty shall be declared to be imposed
       as per the Laws.

44. If a C/m resting periously at the mouth of the pocket and actually
    falls into the pocket for any reason, it shall be considered to
    have been properly pocketed.

Dues and/or Penalties
=====================

45.

   (a) If in a proper/improper stroke a player pockets his Striker
       alone, his turn shall be lost and one of his C/m will be taken
       out as penalty by his opponent for placing. Such penalty C/m
       shall be called "Due".

   (b) If this happens before any of his C/m is pocketed the penalty
       Due shall remain outstanding and shall be taken out as soon as
       it is available.

46. If a player pockets the Striker with his own C/m, the number of C/m
    so pocketed, with a Due C/m, shall be taken out for placing and the
    player shall continue his turn.

47. If a player pockets the Striker with the C/m of his opponent, the
    C/m shall be deemed to have been pocketed. The Due as per Rule
    72(a) shall be taken out for placing and the player shall lose his
    turn.

48. If a player pockets the Striker with C/m of his own and of his
    opponent, the number of his own C/m so pockted, with a Due C/m
    shall be taken out for placing by the opponent and the player shall
    continue his turn.

49. If a player pockets the C/m of his opponent by an improper stroke,
    the C/m so pocketed shall be deemed to have been pocketed. The Due
    shall be taken out for placing by the opponent and the player shall
    lose his turn.

50.

   (a) If a player pockets his own C/m by an improper stroke, the C/m
       so pocketed with a Due C/m shall be taken out for placing by the
       opponent and the player shall lose his turn.

   (b) If a player pockets his own C/m with the Striker by an improper
       stroke, the C/m so pocketed with a Due shall be taken out for
       placing by the opponent and the player shall lose his turn.

51.

   (a) Due or penalty C/m shall be taken out for placing immediately
       after being available, but only after the conclusion of the
       stroke, though it may be during the turn of the same player.

   (b)

     i. In Doubles the Due and/or penalty C/m shall always be taken out
        for placing by the player who is sitting on the right hand side
        of the player having his turn at the time of the availabilityof
        C/m.

     ii. If, however, during the turn of a player, he pockets the C/m
         of the opponent with or without his C/m and the Due and/or
         penalty C/m becomes available, the player himself shall take
         out the C/m for placing.

52.

   (a) If Due and/or penalty C/m is available for placing but
       sufficient space is not available, the player who has to place
       the C/m shall be permitted to do so immediately after space
       becomes available.

   (b) In Doubles, however, if the eligibility to place the Due and/or
       penalty C/m has passed on to the partner, the partner alone
       shall have the right to take out and place the C/m.

53. If space is available for placing the Due and/or penalty C/m, but
    the player who has to place the C/m does not desire to risk a foul,
    he shall lose his chance to place and his claim for placing shall
    stand forfeited.

54. If the space becomes available for placing the Due and/or penalty
    C/m during the turn of the player eligible to place, the placing
    shall be made immediately.

55.

  (a) While placing the Due and/or penalty C/m, if a player places his
      own C/m by mistake, it has to be rectified, if pointed out by
      the Umpire or the opponent. A foul shall be declared as per the
      Laws.

  (b) If not noticed either by the Umpire or the opponent before the
      next stroke is made, the C/m so placed shall be regarded as
      valid.

56. If more than one C/m are to be placed be a player as Due and/or
    penalty, the C/m available shall be placed immediately and rest of
    the C/m have to be placed as soon as available.

57.

  (a) Placing shall be considered as complete once the finger is
      removed from the C/m provided the C/m placed is inside the Outer
      Circle.

  (b) However, while placing Due/penalty C/m, holding of any other C/m
      and/or Striker is not permitted.

58. If a player places or moves the Due and/or penalty C/m away from
    the Outer Circle, he shall be asked to place the C/m inside the
    Outer Circle. A foul shall be declared as per the Laws.

59. While placing the Due and/or penalty C/m the player should not
    move any other C/m and/or Queen. The C/m so placed shall not
    disturb any other C/m and/or Queen. The C/m so placed shall not
    disturb and other C/m. If it so happens, the same shall be
    replaced by the Umpire in the original position, as far as
    possible. A foul shall be declared against the offending player as
    per the Laws.

60. A player may choose to forego the Dua and/or penalty C/m in toto
    only and not partially. Such decision shall be communicated to the
    Umpire within 15 seconds, failing which the right to place the Due
    and/or penalty shall be forfeited.

61. The time limit for placing Due and/or penalty C/m shall be 15
    seconds after the announcement made by the Umpire.

62.

  (a) Due and/or penalty C/m cannot be set off against each other.

  (b) Due and/or penalty C/m shall not be placed covering the Centre
      Circle wholly or partially. If placed, the player shall be asked
      to rectify. A foul shall be declared against the offending
      player as per the Laws.

63. In Doubles, a player shall not take out the penalty and/or Due C/m
    for his partner, who has to place it. In case his partner does not
    find the required C/m in the pockets adjoining his side, he shall
    request the Umpire to provide, specifying the colour and number he
    needs for placing.

64. During the course of the board, if a player gets up from the seat
    during his or his opponent's turn, for any reason, he shall lose
    the board with the number of his C/m and/or Queen lying on the
    board. If the score of the opponent is 22 or more, he shall lose
    the board by the number of C/m only.

Queen
=====

65. A player has the right to pocket the Queen and to cover it provided
    a C/m of his own has already been pocketed.

66. The Queen shall be placed by the Umpire only in the Centre
    Circle. While placing so, however, if shot is automatically formed,
    it cannot be altered.

67. If the Centre Circle is partially or completely covered by other
    C/m, the Queen shall be placed so as to occupy most of the
    uncovered portion or in any position adjacent to the Centre Circle
    in such a way so that it is not easy to pocket for the player
    having his rurn. The placing of the Queen by the Umpire, in such a
    situation, shall be final.

68.

   (a) If the Queen is pocketed before any C/m of the player is
       pocketed, the Queen shall be taken out for placing and the
       player shall lose his turn.

   (b) If a player pockets the Queen, while there is a Due against him,
       the Queen shall be taken out for placing and the player shall
       lose his turn.

   (c) However, if after recovery of Due and/or penalty, all the nine
       C/m are on the C/B, a player shall have the right to pocket the
       Queen and to cover it.

   (d) If, at the break or in a subsequent stroke when all his nine C/m
       are on the C/B, the Queen is pocketed along with the Striker,
       the Queen shall be taken out for placing and a Due shall be
       declared. The player shall lose his turn.

69. If the Queen is pocketed by a stroke and is not covered, the Queen
    shall be taken out for placing. If not noticed by the Umpire or by
    the opponent, before the next stroke is made, the Queen shall be
    recorded as properly covered.

70.

   (a) If the Queen and the C/m of a player are pocketed together in
       one stroke, the Queen shall be considered covered.

   (b) However, at the break and/or any subsequent stroke, when all 9
       C/m of the player are on the C/B, if the Queen and one of the
       C/m are pocketed together, the Queen has to be covered. If more
       than one C/m and the Queen are pocketed together, the Queen
       shall be considered covered.

71.

   (a) If the Queen, C/m and the Striker are pocketed together by a
       proper stroke, the Queen and the C/m so pocketed with an
       additional one as Due, shall be taken out for placing and the
       player shall continue his turn.

   (b) If the Queen, C/m and the Striker are pocketed together by an
       improper stroke, Queen and the C/m so pocketed with the two
       additional C/m as Due shall be taken out for placing and the
       player shall lose his turn.

72.

   (a) If the Queen and the Striker are pocketed together by a proper
       stroke, the Queen shall be taken out for placing by the
       Umpier. An additional C/m shall be taken out for placing and the
       player shall continue his turn.

   (b) If the Queen and the Strkier are pocketed together by an
       improper stroke, the Queen shall be taken out by the Umpire for
       placing. Two additional C/m shall be taken out for placing by
       the opponent and the player shall lose his turn.

73.

   (a) While covering the Queen, if the Striker alone is pocketed, the
       Queen shall be taken out for placing. A C/m of the offending
       player shall be taken out as Due for placing by the opponent and
       the player shall lose his turn.

   (b) While covering the Queen, if the Striker alone is pocketed by an
       improper stroke the Queen shall be taken out for placing. Two
       C/m of the offending player shall be taken out as Due for
       placing by the opponent and the player shall lose his turn.

74.

  (a) While covering the Queen, if a player pockets the Striker along
      with his C/m, the C/m so pocketed plus one C/m as Due shall be
      taken out for placing by the opponent. The player shall,
      however, continue his turn. If in that subsequent stroke no C/m
      of the player is pocketed, the Queen shall not be considered to
      have been covered and it shall be taken out for placing.

  (b) While covering the Queen, if a player pockets the Striker along
      with his C/m by an improper stroke, the C/m so pocketed plus two
      C/m as Due shall be taken out for placing by the opponent and
      the player shall lose his turn.

75.

  (a) While covering the Queen a player pockets the last C/m of his
      own together with the last C/m of his opponent, he shall be
      awarded 3 points. If the score is 22 or more he shall win by 1
      point.

  (b) While covering the Queen, a player pockets the last C/m of his
      own together with the last C/m of his opponent by an improper
      stroke the opponent shall win by 3 points, if the score is 22 or
      more he shall win by 1 point. If demanded, an additional point
      for the improper stoke shall be awarded the opponent.

76.

  (a) While covering the Queen, if a player pockets the last C/m of
      his opponent, he shall lose the board by the number of his C/m
      lying on the C/B together with the points for the Queen. If the
      score of the opponent is 22 or more, he shall lose by the number
      of C/m only.

  (b) While covering the Queen, if a player pockets the last C/m of
      his opponent by an improper stroke, he shall lose the board by
      the number of his C/m lying on the C/B together with the points
      for the Queen. If the score of the opponent is 22 or more, he
      shall lose by the number of C/m only. If demanded, an additional
      point shall be awarded the opponent.

77.

  (a) If a player pockets the Queen along with his last C/m and the
      last C/m of the opponent by a proper stroke, the player shall
      win the board by 3 points. If the score is 22 or more he shall
      win by 1 point.

  (b) If a player pockets the Queen along with his last C/m and the
      last C/m of the opponent by an improper stroke, the opponent
      shall win the board by 3 points. If the score is 22 or more he
      shall lose by 1 point. If demanded, one additional point shall
      be awarded.

78.

  (a) If a player pockets the last C/m of his own and of his opponent
      by a proper stroke while the Queen is on the C/B, the opponent
      whall be awarded 3 points. If the score is 22 or more he shall
      be awareded only 1 point.

  (b) If a player pockets the last C/m of his own and of his opponent
      by an improper stroke while the Queen is on the C/B, the
      opponent shall be awareded 3 points. If the score is 22 or more
      he shall be awareded only 1 point. If demanded, the oppontnent
      shall be awarded one additional point.

79.

  (a) If a player by a proper stoke pockets the last C/m of the
      opponent when the Queen is still on the C/B, he shall lose the
      board by the number of his own C/m lying on the C/B together
      with the points for the Queen. If the opponent's score is 22 or
      more he shall lose by the number of C/m only.

  (b) If a player pockets the last C/m of the opponent by an improper
      stroke when the Queen is on the C/B, he shall lose the board by
      the number of his own C/m lying on the C/B together with the
      points for the Queen. If the opponent's score is 22 or more he
      shall lose the board by the number of C/m only. If demanded, the
      opponent shall be awarded one point in addition.

80.

  (a) If a player pockets his last C/m by a proper stoke leaving the
      Queen on the C/B, he shall lose the board by 3 points. If the
      opponent's score is 22 or more, he shall lose by 1 point.

  (b) If a player pockets his last C/m by an improper stroke leaving
      the Queen on the C/B, he shall lose the board by 3 points. If
      the opponent's score is 22 or more, he shall lose by 1 point. If
      demanded, the opponent shall be awarded one additional point.

81.

  (a) If a player pockets his hast C/m along with the Striker leaving
      the Queen on the C/B by a proper stroke, he shall lose the board
      by 3 points. If the score of the opponent is 22 or more, he
      shall lose by 1 point. One additional point for the pocketed
      Striker shall be awarded if demanded by the opponent.

  (b) If a player pockets his last C/m along with the Striker leaving
      the Queen on the C/B by an improper stroke, he shall lose the
      board by 3 points. If the score of the opponent is 22 or more,
      he shall lose by one point. If demanded, the opponent shall be
      awarded two additional points.


82.

  (a) If a player pockets the Queen, this last C/m, the last C/m of
      his opponent by a proper stroke together with the Striker the
      player shall lose the board by 3 points. If the score of the
      opponent is 22 or more, he shall lose by 1 point. One additional
      point for the pocketed Striker shall be awarded if demanded by
      the opponent.

  (b) If a player pockets the Queen, his last C/m, the last C/m of his
      opponent together with the Striker by an improper stoke, the
      player shall lose the board by 3 points. If the score of the
      opponent is 22 or more, he shall lose by 1 point. If demanded,
      the opponent shall be awarded two additional points.

83.

  (a) If a player pockets the last C/m of his own and of his opponent
      with the Striker by a proper stroke, he shall lose the board by
      one point, if the Queen has been covered by him. One addtional
      point for the pocketed Striker shall be awarded if demanded by
      the opponent.

  (b) If a player pockets the last C/m of his own and of his opponent
      together with the striker by an improper stroke, he shall lose
      the board by one point, if the Queen has been covered by
      him. Two additional points shall be awarded if demanded by the
      opponent.

84.

  (a) If a player pockets the last C/m of his opponent along with the
      Striker leaving the Queen on the C/B by a porper stroke, he
      shall lose the board by the number of his C/m lying on the board
      plus the value of the Queen. If the score is 22 or more, he
      shall lose by the number of C/m only. One additional point for
      the pocketed Striker shall be awarded if demanded by the
      opponent.

  (b) If a player pockets the last C/m of his opponent along with the
      Striker leaving the Queen on the C/B by an improper stroke, he
      shall lose the board by the number of his C/m lying on the board
      plus the value of the Queen. If the socre is 22 or more he shall
      lose by the number of his C/m only. If demanded, the opponent
      shall be awarded two additional points.

85.

  (a) If a player pockets the last C/m of his own and of his opponent
      together with the Striker by a proper stroke, he shall lose the
      board by 3 points, if the Queen has been covered by the
      opponent. One additional point shall be awarded if demanded by
      the opponent.

  (b) If a player pockets the last C/m of his own and of his opponent
      together with the Striker by an improper stroke, he shall lose
      the board by 3 points, if the Queen has been covered by the
      opponent. Two additional points shall be awarded if demanded by
      the opponent.

86. If the Queen resting periously at the mouth of the pocket actually
    falls into the pocket for any reason, it shall be considered to
    have been (duly) pocketed.

87. A player shall not utilise the Due and/or penalty C/m to make a
    shot with the Queen. If utilised, he shall be asked by the Umpire
    to rectify. A foul shall be declared as per the Laws.
