.. -*- coding: utf-8 -*-
.. :Project:   SoL -- Introduction
.. :Created:   gio 6 feb 2020, 09:52:21
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2020, 2022 Lele Gaifax
..

==============
 Introduction
==============

SoL is a web application that allows to manage Carrom events, possibly organized in
championships: over of the years it has become quite articulated and flexible, to handle a wide
spectrum of situations, from simple courtyard tournaments up to international competitions.


Concepts
========

User
  A person who have an account in the system, that manages *players*, *clubs*, *championships*
  and *tournaments*.

  Most users can create their own *clubs*, *championships* and so on, with the notable
  exception of *players*.

  The *administrator* may grant two additional permissions to an user:

  a. the right to :ref:`manage the entities owners <owners admin>`

     This allows to change the *manager* of the entities, even of those not owned by the user
     itself.

  b. the right to :ref:`manage the players <players manager>`

     This allows the insertion of new *players*: without it, one can only subscribe players
     that `already exist` in the database to his new tournaments.

Administrator
  A special super powered *user*, that may change everything in the system.

Manager
  The particular *user* that `owns` an entity (that is, *players*, *clubs*, *tournaments* and
  so on), usually the one that initially created it. While any user can see almost everything,
  an entity can be modified only by the user that `manages` it (or by the *administrator*).

Player
  A person who play Carrom and may participate to *tournaments*.

  They are usually associated to a particular *club* or *federation*.

Competitor
  One (in *singles* events) or more (in *doubles* or *team* events) *players* who are
  subscribed to play in *tournament*.

Club or Federation
  An association of *players*, that may organize *championships*.

Tourney or Tournament
  A Carrom event, taking place at a particular location in a given day.

  They are linked to a *championship* and are thus managed by a particular *club*, although
  they may be hosted by a different one.

Round
  A single “turn” of an event: depending on the number of *competitors*, a *tourney* can be
  composed by several rounds, usually seven.

  Each round involves one or more *matches* between two *competitors*.

Match
  Two *competitors* playing one against the other, in one or more `boards`, until one of them
  accumulate 25 points or the time runs out, whichever comes first.

Championship
  A set of *tourneys*, organized by a given *club*, sharing some common rules in particular the
  kind of *prizes*, so that a `championship ranking` can be computed.

  Most often a championship is `seasonal`, and thus a given club may have many of them, usually
  one `currently active` and the others `historical`. A new tournament may be created only in a
  active championship.

Rating
  A way to compute the `strength` of a *player* thru a `statistical formula`__, mainly used to
  determine the couplings of the initial *rounds* of a *tourney*. It is automatically
  recomputed at the end of each event, considering all the disputed *matches*.

__ https://en.wikipedia.org/wiki/Glicko_rating_system


Minimal setup
=============

The very first thing you need is an `account` on the system, that is registering yourself as a
*user* so that you can :ref:`log in <authentication>` and operate.

To do that, you either contact the `administrator` of the system or use the automatic
:ref:`self registration <signin>` procedure.

As soon you are authorized, these are the required steps to manage your next tournament:

.. contents::
   :local:

1. Choose or create your new club
---------------------------------

First step in setting up a tournament is to open the :ref:`clubs management <clubs management>`
window, either from the main popup menu located in the lower left corner of the application, or
directly from the ``My clubs`` icon in the upper left corner. The latter will list only the
clubs that you can managed, usually the ones owned by you.

Here you must select the club which will be running the event.

If your club is not already in the database please create a new club by clicking ``Add new``
and filling in the necessary information: you will find two important fields, ``Pairing`` and
``Bounties`` which will be useful later. Please fill them in: if your club normally runs small
local tournaments with less than 15 players it is better to choose ``Ranking Order`` and
``Fixed Bounties``, while with more players the other options may be a better fit.

Some setting entered here will be used as default values when creating a new championship.

2. Choose or create a new championship
--------------------------------------

Once you have chosen your club click ``Championships`` in the window's menu, that will open the
:ref:`list of championships <championships management>` managed by that club.

Here you can select the correct championship of which your event is part or create a new
championship by clicking ``Add new``.

When you add a new championship several important fields must be filled in:

Players
  This is where you choose if you are planning a `Singles` or `Doubles` event: 1 player for
  singles, 2 for doubles.

Skip
  This field is only necessary if you want to ignore the worst results in a series of
  tournaments during the year eg. there are 8 events and only the best 6 results will be taken
  into account for the final ranking.

Rating
  The rating is only really important for establishing the pairings of the first few rounds,
  and it is mainly useful for major events. It is based on simple statistics: a person with a
  higher rating will `more probably` win a match against a person with a lower rating. It is
  not based on tournament rankings but only on single matches played.

  There are 4 levels of rating which can be associated to a tournament:

  Level 1
    international events (Eurocup, ICF cup, International Open)

  Level 2
    national events

  Level 3
    regional events

  Level 4
    small local club events

  If you associate a rating to your championship, it will be used as the default choice when
  you will create new related tourneys.

  .. important:: **Please** choose or create the rating at a level as low as
                 possible/reasonable.

  .. hint:: Should you need to create a new rating, *please* give it a clear name, and possibly
            associate it to your club, to make it easily recognizable.

Pairings
  What you enter here will be used as default value when creating a new tourney.

Bounties
  This determine which kind of final prizes will be assigned at the end of the tourneys
  associated with the championship.

3. Create a new tourney
-----------------------

Once you have chosen the right championship click on ``Tourneys`` in the window's menu, that
will open the :ref:`list of tourneys <tourneys management>` associated with that championship.

At this point you are ready to add a new tournament: click ``Add new`` and fill in the details,
some will be already filled in on the basis of the info provided by you in Club and
Championship.

.. note:: If you put a date in the future the event will disappear from the main list and you
          have to find it using the date search button.

Choose if there will be a final and what kind, choose the time limit of matches and pre-alarm
(for the clock).

`Delay top players pairing` is useful mainly for events with few players where you don’t want
the top two to meet in the first rounds and where you have chosen ``ranking order`` as the
system for pairing.

`Same nation pairing` is to avoid people from the same country meeting in the first round or
two – useful mainly for international events.

`Drop outs` is useful - if a player is forced to abandon a tournament his opponents will not
lose out on the bucholz (see :ref:`details <drop-outs>`).

4. Add participants
-------------------

When you are done, click on ``Details`` that will open the :ref:`tourney management window
<tourney management>`, and start choosing the participants from the database: click on ``Show
all players`` if this is a new event. Use the search option to find players based on name,
country etc.

To add new players you must have the :ref:`manage the players <players manager>` permission or
ask your national Manager to add the names prior to the event.

You are now ready to generate the first round and start playing!

.. rubric:: Notes

.. important:: Remember to click on ``Save`` after every action you take.

.. hint:: You can print score cards with or without names, you can print ID tags and souvenir
          tags at the end of the event – you can choose to see separate final ranking for
          juniors, nationality and women.

.. hint:: In each window ``?`` icon in its top right border will take you to the relevant
          section of the detailed user manual.
