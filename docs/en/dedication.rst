.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   dom 09 nov 2008 19:18:57 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2008-2010, 2014 Lele Gaifax
..

============
 Dedication
============

For the second time in my life I'm *using* Carrom as a therapy, to
fight against blue times. It works great!

SoL is partially due to Joseph Leaded, an illiterate owner of a local
software house, who fired me after more than ten years of
collaboration and commitment on my part, stealing my work without
paying a dime for the last seven months. Without his rudeness I'd be
still injuring my brain writing silly Delphi apps on Window$. I *had*
to take a vacancy, to enjoy again the art of programming.

Seriously, I dedicate this work to my brother Fausto, who is surely
building perfect Carrom boards and teaching good shots in his
`निर्वाण`_: without him, neither Scarry nor SoL would have been
written.

.. _निर्वाण: http://en.wikipedia.org/wiki/Nirvana

Acknowledgements
================

I wanna say "Thank you!" to the various communities that made this
project easier and delightful:

Python_
  Simple things are simple, difficult ones are possible. With joy!

SQLAlchemy_
  Using relational databases at their full strength with a breeze of
  fresh air.

Pyramid_
  Programming for the web tends to tediousness… without a good
  and versatile framework.

ExtJS_
  I'm constantly surprised seeing how effective may be a fullfledged
  GUI inside a (good) web browser.

SCR_
  Good friends are a good thing!

.. _python: http://www.python.org/
.. _sqlalchemy: http://www.sqlalchemy.org/
.. _pyramid: http://www.pylonsproject.org/
.. _extjs: http://www.sencha.com/products/extjs/
.. _scr: https://www.facebook.com/Scarambol
