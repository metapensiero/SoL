.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   gio 7 mag 2020, 08:47:46
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2020, 2024 Lele Gaifax
..

================
 Privacy policy
================

To comply with the `General Data Protection Regulation`__ (GDPR), here is some information
about how SoL handles personal data.

__ https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679

Personal data controller
========================

For the *official* ``SoL`` instance ``https://sol5.metapensiero.it/`` the data protection
officer is reachable at the email address ``lele@metapensiero.it``.

Being ``SoL`` `free software`__, anybody can and is allowed to create further instances of the
application, that are beyond the control of the aforementioned responsible person: in no way
the creation of further instances concerns the scope of the data, since the creation of a new
instance merely involves copying the ``SoL`` source code.

__ https://en.wikipedia.org/wiki/Free_software

Collected personal data
=======================

Players
-------

Essential information:

- first name
- last name

Optional information:

- nick name
- sex
- birthdate
- nationality
- citizenship
- email address
- language
- portrait

System users
------------

Essential information:

- first name
- last name
- email address
- password

Optional information:

- language

Purpose of personal data processing
===================================

Player information is collected exclusively for the purpose of offering the Carrom tournaments
management service. The `first name` and `last name` are therefore essential to be able to
distinguish the players, while other optional information, when available, serves to allow
further *ranking* by gender (`sex`, `birthdate` and `nationality`), or to send messages related
to the tourneys a player participates to (`email` and `language`).

Users data is used to acknowledge who may enter in the system to create and manage his own
Carrom tournaments.

Access to personal data
=======================

Access to personal data is granted to the *users* of the system, who organize Carrom
tournaments using ``SoL``.

Some of the player's personal data can be reported in the public interface
(``https://sol5.metapensiero.it/lit/``), viewable by anonymous visitors. The single player can
but to require that one's personal details be *blurred*, making it so *unrecognizable*.

Under no circumstances will the data collected be sold or provided to third parties.

The actual storage of data of the *official* ``SoL`` instance is within the European Union.

Rights
======

Players voluntarily provide their personal data, and to be able to participate in a
Carrom's tournament organized with ``SoL`` must necessarily allow the use of at least the
*essential information*.

Any player has the right to:

- request the rectification of his personal data
- deny consent to the *recognition* of his data in the public interface

Exclusively for the *official* ``SoL``` instance (``https://sol5.metapensiero.it/``), requests
for rectification and denial may be sent to the responsible, ``lele@metapensiero.it``.
