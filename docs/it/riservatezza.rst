.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   gio 7 mag 2020, 09:05:15
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2020, 2024 Lele Gaifax
..

==========================
 Politica di riservatezza
==========================

Nel rispetto del `Regolamento Generale sulla Protezione dei Dati`__ (``GDPR``), ecco qualche
informazione su come ``SoL`` tratta i dati personali.

__ https://eur-lex.europa.eu/legal-content/IT/TXT/HTML/?uri=CELEX:32016R0679

Controllore dati personali
==========================

Per quanto riguarda l'istanza di ``SoL`` *ufficiale* ``https://sol5.metapensiero.it/`` il
responsabile della protezione dei dati è raggiungibile all'indirizzo email
``lele@metapensiero.it``.

Essendo ``SoL`` un `software libero`__, chiunque è in grado ed autorizzato a creare ulteriori
istanze dell'applicazione, che esulano dal controllo del succitato responsabile: in nessun caso
la creazione di ulteriori istanze riguarda l'ambito dei dati, dal momento che la creazione di
una nuova istanza comporta puramente la copia del codice sorgente di ``SoL``.

__ https://it.wikipedia.org/wiki/Software_libero

Dati personali collezionati
===========================

Giocatori
---------

Informazioni essenziali:

- nome
- cognome

Informazioni facoltative:

- soprannome
- sesso
- data di nascita
- nazionalità
- cittadinanza
- indirizzo email
- lingua
- ritratto

Utenti del sistema
------------------

Informazioni essenziali:

- nome
- cognome
- indirizzo email
- password

Informazioni facoltative:

- lingua

Scopo del trattamento dei dati personali
========================================

I dati dei giocatori vengono raccolti esclusivamente al fine di offrire il servizio di gestione
dei tornei di Carrom. Il `nome` e il `cognome` sono pertanto indispensabili per poter
distinguere i giocatori, mentre le altre informazioni facoltative, quando disponibili, servono
per consentire ulteriori *classifiche* per genere (`sesso`, `data di nascita` e `nazionalità`),
o per consentire l'invio di messaggi relativi ai tornei a cui il giocatore partecipa (`email` e
`lingua`).

I dati degli utenti consentono di riconoscere chi può accedere al sistema per poter creare e
gestire i propri tornei di Carrom.

Accesso ai dati personali
=========================

L'accesso ai dati personali è garantito agli *utenti* del sistema, che organizzano tornei di
Carrom usando ``SoL``.

Alcuni dei dati personali dei giocatori possono essere riportati nell'interfaccia pubblica
(``https://sol5.metapensiero.it/lit/``), consultabile anonimamente. Il singolo giocatore può
però richiedere che le proprie generalità vengano *offuscate*, rendendolo in tal modo
*irriconoscibile*.

In nessun caso i dati raccolti vengono venduti o forniti a terze parti.

La memorizzazione dei dati relativi all'istanza di ``SoL`` *ufficiale*
(``https://sol5.metapensiero.it/``) avviene nell'ambito dell'Unione Europea.

Diritti
=======

I giocatori forniscono volontariamente i propri dati personali, e per poter partecipare ad un
torneo di Carrom organizzato con ``SoL`` devono necessariamente consentire l'uso almeno delle
*informazioni essenziali*.

Ogni giocatore ha il diritto di:

- richiedere la rettifica dei propri dati personali
- negare il consenso alla *riconoscibilità* dei propri dati nell'interfaccia pubblica

Per quanto riguarda esclusivamente l'istanza di ``SoL`` *ufficiale*
(``https://sol5.metapensiero.it/``), le richieste di rettifica e diniego possono essere inviate
al responsabile, ``lele@metapensiero.it``.
