.. -*- coding: utf-8 -*-
.. :Project:   SoL -- SoLitude variant
.. :Created:   dom 5 apr 2020, 09:19:34
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2020, 2021 Lele Gaifax
..

================================
 Gioco del Carrom in solitudine
================================

Durante i primi mesi dell'anno 2020 l'intera umanità ha affrontato un'impetuosa `emergenza
sanitaria`__: probabilmente per la prima volta nella storia, tutte le attività umane in tutte
le aree popolate del pianeta sono state (s)travolte, e mentre scrivo nessuno sa ancora di
preciso quanto potrà durare, né tanto meno quali potranno essere le conseguenze sociali ed
economiche a lungo termine.

Il mio pensiero va ai familiari delle centinaia di migliaia di vittime e al tempo stesso a
tutte le persone che, mettendo in secondo piano la loro stessa salute e a repentaglio quella
dei loro cari, fronteggiano la pandemia.

__ https://it.wikipedia.org/wiki/Pandemia_di_COVID-19_del_2019-2020


.. _corona carrom:

Corona Carrom
=============

Dal momento che sostanzialmente l'unica difesa che possiamo applicare è quella del
`distanziamento sociale`__ e che tutte le attività ludiche di gruppo sono state sospese a tempo
indeterminato, i Carrommisti si sono adeguati, inventandosi diverse modalità competitive.

__ https://it.wikipedia.org/wiki/Pandemia_di_COVID-19_del_2019-2020#Distanziamento_sociale

**State a casa e fate pratica!**


Regole del gioco comuni a tutte le modalità
-------------------------------------------

La partita deve essere registrata **live** sul gruppo appropriato di ``Facebook`` o di un altro
social media:

- vai al gruppo che ti è stato assegnato
- disponi il tuo cellulare in posizione orizzontale ed assicurati che la ripresa delle partite
  avvenga orizzontalmente
- fai clic su ``Live Video`` nella parte superiore della pagina
- fai clic su ``Avvia video`` quando sei pronto per giocare

1. Disponi le pedine sul tavolo. Durante il gioco devi imbucare tutte le pedine sia
   bianche che nere secondo le regole stabilite per lo specifico torneo

2. Spacca e inizia a contare il numero di colpi errati, cioè che non imbucano nessuna pedina.
   Conta gli errore con un contatore, fagioli o qualsiasi altro sistema che sia visibile a
   tutti gli spettatori

3. Segui la regola ufficiale di *Due* e *Double Due* (*Penalità*):

   *Due*
     estrai 1 pedina e posizionala nel cerchio sul lato opposto rispetto a te e aggiungi 1 al
     conteggio

   *Double Due*
      estrai 2 pedine e posizionale orizzontalmente in modo che si tocchino sul lato opposto
      del cerchio senza aggiungere al conteggio

4. Il primo board termina quando hai imbucato tutte le 18 pedine e la regina: annota il numero
   di errori che hai commesso per finire il primo board da qualche parte, in modo che sia ben
   visibile a tutti

5. Ripeti quanto sopra per il numero di board previsti

6. Alla fine, visita l'URL della pagina SoL che ti è stato inviato via email e inserisci i
   conteggi nel modulo e confermalo
