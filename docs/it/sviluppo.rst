.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   lun 31 mar 2014 19:52:22 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014, 2018, 2020, 2024 Lele Gaifax
..

==========
 Sviluppo
==========

Lo sviluppo di SoL avviene in un repository git__ ospitato da GitLab__.

Puoi visitare la pagina con `l'attività`__ per vedere cosa è stato fatto di recente.

Se sei uno sviluppatore, sei incoraggiato a `creare un fork`__ per adattare e migliorare SoL e
sarò più che contento di integrare le modifiche che potrai contribuire.

C'è anche una `documentazione tecnica`__ estratta automaticamente dai sorgenti.

__ http://git-scm.com/
__ https://gitlab.com/metapensiero/SoL
__ https://gitlab.com/metapensiero/SoL/activity
__ https://docs.gitlab.com/ee/workflow/forking_workflow.html
__ ../index.html


Donazioni
=========

Da sempre\ [#]_ ho sostenuto tutte le spese di mantenimento di SoL, sia in termini di studio e
sviluppo del software, sia per quanto riguarda il costo di hosting del servizio, usabile e
consultabile gratuitamente.

Da qualche tempo la `ECC`__ sponsorizza lo sviluppo di SoL, sostenendone parte delle spese
vive di gestione del server.

Se usi SoL per organizzare tornei, in particolare quelli di rilevanza nazionale, per favore
**considera** la possibilità di fare una donazione contribuendo al suo sviluppo e
sostentamento, affinché il servizio possa continuare anche in futuro.

.. [#] Ho cominciato a scrivere ``Scarry`` in ottobre del 1999, mentre ``SoL`` ha visto la luce
       poco meno di dieci anni più tardi!

__ http://www.european-carrom-confederation.com/pageID_1469902.html


Liberapay
---------

.. raw:: html

   <script src="https://liberapay.com/lele/widgets/button.js"></script>
   <noscript>
     <a href="https://liberapay.com/lele/donate">
       <img alt="Fai una donazione tramite Liberapay"
            src="https://liberapay.com/assets/widgets/donate.svg">
     </a>
   </noscript>


PayPal
------

.. raw:: html

   <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
     <input type="hidden" name="cmd" value="_s-xclick" />
     <input type="hidden" name="hosted_button_id" value="WEWAYTXBYECKL" />
     <input type="image" src="https://www.paypalobjects.com/it_IT/IT/i/btn/btn_donate_SM.gif"
            border="0" name="submit" title="PayPal - The safer, easier way to pay online!"
            alt="Fai una donazione con il pulsante PayPal" />
     <img alt="" border="0" src="https://www.paypal.com/it_IT/i/scr/pixel.gif" width="1"
          height="1" />
   </form>
