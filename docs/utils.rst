.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mer 09 lug 2014 10:13:27 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014 Lele Gaifax
..

======================================
 :mod:`sol.models.utils` -- Utilities
======================================

.. automodule:: sol.models.utils

.. autofunction:: asunicode
.. autofunction:: normalize
.. autofunction:: njoin
.. autofunction:: entity_from_primary_key
.. autofunction:: table_from_primary_key
