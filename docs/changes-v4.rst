.. -*- coding: utf-8 -*-
.. :Project:   SoL — Changelog of version 4
.. :Created:   ven 31 mag 2024, 17:42:42
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2024 Lele Gaifax
..

Changelog of version 4
----------------------

4.21 (2023-03-29)
~~~~~~~~~~~~~~~~~

* Minor stylistic tweaks to the digital scorecard

* Some more fixes to handling final matches with more than 9 games


4.20 (2023-03-21)
~~~~~~~~~~~~~~~~~

* Sort the participants printout by competitor's name, matching the order of the correspondent
  panel

* Rectify the storage of the scores of final matches when they take more than 9 games

* Make it easier to understand which competitor is going to break in new game, highlighting his
  name


4.19 (2023-02-19)
~~~~~~~~~~~~~~~~~

* Slim down the tourney's and championship's Lit pages by hiding the mostly "technical" details
  table within a <details> element


4.18 (2023-02-17)
~~~~~~~~~~~~~~~~~

* Highlight the breaker competitor in the tourney match details


4.17 (2023-01-23)
~~~~~~~~~~~~~~~~~

* New tourney's playbill printout

* Make it possible to reset queen assignment in the digital scorecard


4.16 (2023-01-10)
~~~~~~~~~~~~~~~~~

* When printing the carromboard labels for a tournament, ask in advance how many boards so
  that one can prepare the labels even before having the complete list of participants

4.15 (2023-01-08)
~~~~~~~~~~~~~~~~~

* Further embellishments to the Lit tournament views


4.14 (2023-01-07)
~~~~~~~~~~~~~~~~~

* Minor embellishments to the Lit tournament views

* Fix a rendering error when looking at the matches of a particular player in Lit

* Repair editability of the “previous championship” in the championship edit form


4.13 (2023-01-06)
~~~~~~~~~~~~~~~~~

* Downgrade metapensiero.extjs.desktop back to 2.0, to restore compatibility with Python 3.8


4.12 (2023-01-05)
~~~~~~~~~~~~~~~~~

* Easier search by users name: when applying a basic (i.e., “contains”) filter on first name
  *or* last name *or* email address, actually search in all those fields at the same time

* Substantially improve the Lit user experience on mobile devices

* Add a Lit view for a match detailed results

* Expose the countdown on the digital scoreboard


4.11 (2022-11-20)
~~~~~~~~~~~~~~~~~

* Easier search by players name: when applying a basic (i.e., “contains”) filter on first name
  *or* last name, actually search in both fields at the same time

* Online tournaments do not alter the associated ratings anymore

* Tweak the ``dazed`` and ``staggered`` pairing methods to be slightly more fair (issue
  `#15`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/15


4.10 (2022-07-18)
~~~~~~~~~~~~~~~~~

* Fix computation of bucholz when a retirements policy is in effect

* Fix computation of the ranking of "non current" rounds

* Fix update of matches sidebar when deleting a final round


4.9 (2022-04-10)
~~~~~~~~~~~~~~~~

* Disallow insertion of total scores exceeding 25 points in the self-compilation
  match form

* Fix computation of total score in the scoreboard detailed results


4.8 (2022-03-11)
~~~~~~~~~~~~~~~~

* Better strategy to assign boards in small tournaments (issue `#34`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/34

* Notify the user playing against Phantom in training tournaments (issue `#35`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/35

* Use less ambiguous "register" in the login panel (issue `#30`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/30

* Handle the "suicide" case in self-compilation match form (issue `#29`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/29

* Allow unlimited boards in final matches (issue `#31`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/31


4.7 (2021-10-24)
~~~~~~~~~~~~~~~~

* Refine the self-insertion of match results:

  - avoid ending the insertion by mistake (issue `#24`__)
  - always present the *refresh* action in the matches panel (issue `#25`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/24
  __ https://gitlab.com/metapensiero/SoL/-/issues/25

* Promote the SoL issue tracker in the main menu (issue `#26`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/26

* Fix glitch entering scoreboard detailed results (issue `#27`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/27


4.6 (2021-05-30)
~~~~~~~~~~~~~~~~

* Fix defective Italian translation


4.5 (2021-04-24)
~~~~~~~~~~~~~~~~

* Fix glitch in English messages


4.4 (2021-04-21)
~~~~~~~~~~~~~~~~

* Fix issue `#21`__, due to sloppy coding

  __ https://gitlab.com/metapensiero/SoL/-/issues/21

* Rewrite naïve implementation of the "all against all" turns generator, using the `circle
  method`__ algorithm

  __ https://en.wikipedia.org/wiki/Round-robin_tournament#Circle_method


4.3 (2021-04-20)
~~~~~~~~~~~~~~~~

* Really fix issue `#18`__

  __ https://gitlab.com/metapensiero/SoL/-/issues/18


4.2 (2021-04-20)
~~~~~~~~~~~~~~~~

* Use a less confusing tooltip for the send training board emails action (see issue `#16`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/16

* Possibly advance the current turn of the "all against all" Corona Carrom tournament when the
  ranking is updated (see issues `#18`__ and `#19`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/18
  __ https://gitlab.com/metapensiero/SoL/-/issues/19


4.1 (2021-04-19)
~~~~~~~~~~~~~~~~

* New "all against all" mode for Corona Carrom tournaments


4.0rc4 (2020-05-18)
~~~~~~~~~~~~~~~~~~~

* Fix typo that prevented the automatic backup at login time


4.0rc3 (2020-05-18)
~~~~~~~~~~~~~~~~~~~

* Slightly improved rendering of auto-compile scorecards on desktop browsers

* New actions in the matches panel to open the auto-compile scorecards, when email does not
  work

* New action on the tourneys management to create a knockout tourney from a previous Swiss one


4.0rc2 (2020-05-12)
~~~~~~~~~~~~~~~~~~~

* Minor fixes to English grammar in the user manual

* Fix boolean filters


4.0rc1 (2020-05-11)
~~~~~~~~~~~~~~~~~~~

* Minor tweak the training board results edit window, showing the average misses count


4.0b14 (2020-05-09)
~~~~~~~~~~~~~~~~~~~

* Complete the new boards results edit window, implementing the "training" variant


4.0b13 (2020-05-09)
~~~~~~~~~~~~~~~~~~~

* Fix Lit view of training tournaments


4.0b12 (2020-05-07)
~~~~~~~~~~~~~~~~~~~

* Refine "knockout" system couplings

* New "boards" table, to store matches details, generalizing previous training-boards only
  solution


4.0b11 (2020-04-17)
~~~~~~~~~~~~~~~~~~~

* Implement the "knockout" system, the last long-standing requested feature for v4, yay!


4.0b10 (2020-04-14)
~~~~~~~~~~~~~~~~~~~

* Fix deployment issues


4.0b9 (2020-04-14)
~~~~~~~~~~~~~~~~~~

* Fix deployment issues


4.0b8 (2020-04-14)
~~~~~~~~~~~~~~~~~~

* New optional "social site" URL on tournaments

* Store all boards misses, not just the totals


4.0b7 (2020-04-09)
~~~~~~~~~~~~~~~~~~

* Show both the scores and the errors in the training tournament's Lit view


4.0b6 (2020-04-08)
~~~~~~~~~~~~~~~~~~

* Fix bug that allowed the self-insertion to only one of the competitors...


4.0b5 (2020-04-08)
~~~~~~~~~~~~~~~~~~
:note: one month of captivity...

* Other minor tweaks to "Corona Carrom" management


4.0b4 (2020-04-07)
~~~~~~~~~~~~~~~~~~

* Minor tweaks to "Corona Carrom" management


4.0b3 (2020-04-05)
~~~~~~~~~~~~~~~~~~

* Restore "email" and "language" on players, removed in 4.0a5

* Add support for "Corona Carrom", “El Carrom en los tiempos del Covid-19”


4.0b2 (2020-02-15)
~~~~~~~~~~~~~~~~~~

* Highlight winners in the results printout, as suggested by Carlito

* New "donations" section in the user's manuals (still draft!)


4.0b1 (2020-02-10)
~~~~~~~~~~~~~~~~~~

* New introductory chapter in the user manual, thanks to Elisa for the preliminary text

* New "world" fake country and icon, for international federations

* Add an entry in the main menu to change account's UI language

* Take into account the selected round when printing tourney's matches, for consistency with
  the results printout

* Use darkblue instead of red to highlight winners, as red may suggest an error condition


4.0a10 (2020-02-06)
~~~~~~~~~~~~~~~~~~~

* Add a rating on the clubs, used as default when creating new associated championships

* Clearer identification of ratings, showing their level and associated club, if any


4.0a9 (2020-02-05)
~~~~~~~~~~~~~~~~~~

* Show the user's email in the "owner" lookup, to avoid name clashes

* Fix serialization of the new hosting club tourney's attribute

* New button to start the countdown after 60 seconds

* Fix the actions deactivation logic based on the owner id for new records


4.0a8 (2020-02-01)
~~~~~~~~~~~~~~~~~~

* Add a rating on the championships, used as default when creating new associated tournaments


4.0a7 (2020-01-31)
~~~~~~~~~~~~~~~~~~

* Revise the obfuscation algorithm of player names, using an hash of the original one instead
  of simple truncation, to avoid conflicts; also, from now on it gets applied also to the
  exported streams

* Highlight the not-yet-scored matches in the tourney management window

* Allow emblems and portraits up to 512Kb in size


4.0a6 (2020-01-29)
~~~~~~~~~~~~~~~~~~

* Nicer rendering of the main Lit page

* Simpler way to open the Lit page of a tourney from its management window

* Allow to save partial results, to be on the safe side when there are lots of boards

* Show the "hosting club" on all printouts, if present


4.0a5 (2020-01-25)
~~~~~~~~~~~~~~~~~~

* Remove "email", "language" and "phone" from players data

* Remove player's rate from participants printout

* Omit the player's club in the ranking printout for international tourneys

* Add the player's nationality in matches and results printouts

* Add an "hosting club" to tournaments


4.0a4 (2020-01-18)
~~~~~~~~~~~~~~~~~~

* New association between clubs and users: now a user may add a
  championship/tourney/rating/player only to clubs he either owns or is associated with

* Add a link to send an email to the instance' admin on the login panel


4.0a3 (2020-01-13)
~~~~~~~~~~~~~~~~~~

* Use a three-state flag for the player's *agreed privacy*: when not explicitly expressed, SoL
  assumes they are publicly discernible if they participated to tournaments after January 1,
  2020

* Player's first and last names must be longer that one single character


4.0a2 (2020-01-11)
~~~~~~~~~~~~~~~~~~

* Fix issue with UI language negotiation

* Use the better maintained `Fomantic-UI`__ fork of `Semantic-UI`__ in the “Lit” interface

__ https://fomantic-ui.com/
__ https://semantic-ui.com/

* New tournaments *delay compatriots pairing* option

* Technicalities:

  * Official repository is now https://gitlab.com/metapensiero/SoL

  * NixOS__ recipes (thanks to azazel@metapensiero.it)

__ https://nixos.org/


4.0a1 (2018-08-06)
~~~~~~~~~~~~~~~~~~

.. warning:: Backward **incompatible** version

   This release uses a different algorithm to crypt the user's password: for this reason
   previous account credentials cannot be restored and shall require manual intervention.

   It's **not** possible to *upgrade* an existing SoL3 database to the latest version.

   However, SoL4 is able to import a backup of a SoL3 database made by ``soladmin backup``.

* Different layout for matches and results printouts, using two columns for the competitors to
  improve readability (suggested by Daniele)

* New tournaments *retirements policy*

* New "women" and "under xx" tourney's ranking printouts

* New “self sign up” procedure

* New “forgot password” procedure

* New "agreed privacy" on players

* Somewhat prettier “Lit” interface, using `Semantic-UI tables`__

* Technicalities:

  * Development moved to GitLab__

  * Officially supported on Python 3.6 and 3.7, not anymore on <=3.5

  * Shiny new pytest-based tests suite

  * Uses `python-rapidjson`__ instead `nssjson`__, as I officially declared the latter as
    *abandoned*

  * Uses `PyNaCl`__ instead of `cryptacular`__, as the former is much better maintained

  * "Users" are now a separated entity from "players": now the login "username" is a mandatory
    email and the password must be longer than **five** characters (was three before)


__ https://semantic-ui.com/collections/table.html
__ https://gitlab.com/metapensiero/SoL
__ https://pypi.org/project/python-rapidjson/
__ https://pypi.org/project/nssjson/
__ https://pypi.org/project/PyNaCl/
__ https://pypi.org/project/cryptacular/
