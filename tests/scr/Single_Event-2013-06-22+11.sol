clubs:
- {couplings: serial, description: Polish Carrom Association, guid: a68029446af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: POL, prizes: fixed, siteurl: www.carrom.pl}
- {couplings: serial, description: Villa Loto Carrom Club, emblem: ccvl.png, guid: a67d205a6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ITA, prizes: fixed}
- {couplings: dazed, description: Federazione Italiana Carrom, emblem: fic.png, guid: a67d9b0c6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ITA, prizes: fixed, siteurl: 'http://www.carromitaly.com/'}
- {couplings: serial, description: Carrom Club Milano, emblem: ccm.png, guid: a67d5c0a6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ITA, prizes: fixed, siteurl: 'http://www.carromclubmilano.it/'}
- {couplings: serial, description: Swiss Carrom Association, guid: a67e89cc6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: CHE, prizes: fixed, siteurl: www.carrom.ch}
- {couplings: serial, description: Carrom Club Verona, guid: a67ec5046af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ITA, prizes: fixed}
- {couplings: serial, description: EuroCup, guid: a67eff386af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', prizes: fixed}
- {couplings: serial, description: United Kingdom Carrom Federation, guid: a67f40746af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: GBR, prizes: fixed}
- {couplings: serial, description: French Carrom Federation, guid: a67f7b026af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: FRA, prizes: fixed, siteurl: www.carrom.net}
- {couplings: serial, description: German Carrom Federation, guid: a67fb55e6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: DEU, prizes: fixed, siteurl: www.carrom.de}
- {couplings: serial, description: Slovenia, guid: a67fef246af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: SVN, prizes: fixed}
- {couplings: serial, description: Czech Carrom Association, guid: a6809fb46af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: CZE, prizes: fixed, siteurl: www.carrom.cz}
- {couplings: serial, description: Italian Carrom Federation, guid: a680dc366af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ITA, prizes: fixed, siteurl: www.carromitaly.com}
- {couplings: serial, description: Swedish Carrom Federation, guid: a681207e6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: SWE, prizes: fixed}
- {couplings: serial, description: Spanish Carrom Association, guid: a68171006af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ESP, prizes: fixed}
players:
- {citizenship: true, club: 2, firstname: Fabio, guid: a68e5eba6af811e3bee53085a99ccac7,
  lastname: Tomasi, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: ftomasi.png, sex: M}
- {citizenship: true, club: 3, firstname: Gianluca, guid: a691e2246af811e3bee53085a99ccac7,
  lastname: Cristiani, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: gianluca.jpg, sex: M}
- {citizenship: true, club: 3, firstname: Rodolfo, guid: a692707c6af811e3bee53085a99ccac7,
  lastname: Donnini, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: rodolfo.jpg, sex: M}
- {citizenship: true, club: 3, firstname: Paolo, guid: a69399526af811e3bee53085a99ccac7,
  lastname: Martinelli, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: Paolomart.jpg, sex: M}
- {citizenship: true, club: 3, firstname: Stefano, guid: a695f60c6af811e3bee53085a99ccac7,
  lastname: Stucchi, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: Stucchi.jpg, sex: M}
- {citizenship: true, club: 4, firstname: Paolo, guid: a697a4de6af811e3bee53085a99ccac7,
  lastname: Ometto, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: paolo.jpg, sex: M}
- {citizenship: true, club: 4, firstname: Vittorio, guid: a69ab1a66af811e3bee53085a99ccac7,
  lastname: Canisi, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: vittorio.jpg, sex: M}
- {citizenship: true, club: 4, firstname: Stefano, guid: a69f74e86af811e3bee53085a99ccac7,
  lastname: Fabiano, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  nickname: stefano, sex: M}
- {citizenship: true, club: 5, firstname: Lorenzo, guid: a6ad80a66af811e3bee53085a99ccac7,
  lastname: Hurlimann, modified: !!timestamp '2013-12-22 11:03:13', nationality: CHE,
  portrait: lorenzo.jpg, sex: M}
- {citizenship: true, club: 3, firstname: Elisa, guid: a6b21d326af811e3bee53085a99ccac7,
  lastname: Zucchiatti, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  nickname: elisa, portrait: elisa.jpg, sex: F}
- {citizenship: true, club: 3, firstname: Giorgio, guid: a71151bc6af811e3bee53085a99ccac7,
  lastname: Balicchi, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  sex: M}
- {citizenship: true, club: 3, firstname: Francesco, guid: a718d4506af811e3bee53085a99ccac7,
  lastname: Pedicini, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  sex: M}
- {citizenship: true, club: 6, firstname: Daniele, guid: a77bd87a6af811e3bee53085a99ccac7,
  lastname: Da Fatti, modified: !!timestamp '2013-12-22 11:03:14', nationality: ITA,
  nickname: daniele, sex: M}
- {citizenship: true, club: 7, firstname: Hans, guid: a7a091606af811e3bee53085a99ccac7,
  lastname: Faller, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Armin, guid: a7a124fe6af811e3bee53085a99ccac7,
  lastname: Steiner, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Frank, guid: a7a1b6d06af811e3bee53085a99ccac7,
  lastname: Fehr, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU, sex: M}
- {citizenship: true, club: 7, firstname: Jan, guid: a7a41e166af811e3bee53085a99ccac7,
  lastname: Geister, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Andreas, guid: a7a5c20c6af811e3bee53085a99ccac7,
  lastname: Hurtig, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 8, firstname: Abdul, guid: a7a8ccb86af811e3bee53085a99ccac7,
  lastname: Khan, modified: !!timestamp '2013-12-22 11:03:14', nationality: GBR, sex: M}
- {citizenship: true, club: 8, firstname: Anhar, guid: a7ae92f66af811e3bee53085a99ccac7,
  lastname: Ali, modified: !!timestamp '2013-12-22 11:03:14', nationality: GBR, sex: M}
- {citizenship: true, club: 7, firstname: Bernhard, guid: a7b2c9d46af811e3bee53085a99ccac7,
  lastname: Sander, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 9, firstname: Radjou, guid: a7b6e9386af811e3bee53085a99ccac7,
  lastname: Venou, modified: !!timestamp '2013-12-22 11:03:14', nationality: FRA,
  sex: M}
- {citizenship: true, club: 7, firstname: Fabian, guid: a7b7fa446af811e3bee53085a99ccac7,
  lastname: Pereira, modified: !!timestamp '2013-12-22 11:03:14', nationality: FRA,
  sex: M}
- {citizenship: true, club: 7, firstname: Armin, guid: a7b8999a6af811e3bee53085a99ccac7,
  lastname: Schneider, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 10, firstname: Gunter, guid: a7b92d886af811e3bee53085a99ccac7,
  lastname: Seibert, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Steffen, guid: a7bb8df86af811e3bee53085a99ccac7,
  lastname: Eder, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU, sex: M}
- {citizenship: true, club: 7, firstname: Pierre, guid: a7c038766af811e3bee53085a99ccac7,
  lastname: Dubois, modified: !!timestamp '2013-12-22 11:03:15', nationality: FRA,
  sex: M}
- {citizenship: true, club: 8, firstname: Mukith, guid: a7c4dbd86af811e3bee53085a99ccac7,
  lastname: Hussain, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR,
  sex: M}
- {citizenship: true, club: 7, firstname: Ala, guid: a7c569686af811e3bee53085a99ccac7,
  lastname: Miah, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR, sex: M}
- {citizenship: true, club: 7, firstname: Nitesh, guid: a7c6900e6af811e3bee53085a99ccac7,
  lastname: Sinha, modified: !!timestamp '2013-12-22 11:03:15', nationality: POL,
  nickname: Nick, sex: M}
- {citizenship: true, club: 11, firstname: Thangaraj, guid: a7ca16d46af811e3bee53085a99ccac7,
  lastname: Kuppusamy, modified: !!timestamp '2013-12-22 11:03:15', nationality: SVN,
  sex: M}
- {citizenship: true, club: 7, firstname: Andrea, guid: a7ce242c6af811e3bee53085a99ccac7,
  lastname: Martinelli, modified: !!timestamp '2013-12-22 11:03:15', nationality: ITA,
  sex: M}
- {citizenship: true, club: 8, firstname: Sunahar, guid: a7cf45aa6af811e3bee53085a99ccac7,
  lastname: Ali, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR, sex: M}
- {citizenship: true, club: 7, firstname: Shaheen, guid: a7cfda066af811e3bee53085a99ccac7,
  lastname: Miah, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR, sex: M}
- {citizenship: true, club: 8, firstname: Karnal, guid: a7d06bf66af811e3bee53085a99ccac7,
  lastname: Abdin, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR,
  sex: M}
- {citizenship: true, club: 7, firstname: Viktor, guid: a7d2c5ea6af811e3bee53085a99ccac7,
  lastname: Baumgartner, modified: !!timestamp '2013-12-22 11:03:15', nationality: CHE,
  sex: M}
- {citizenship: true, club: 10, firstname: Peter, guid: a7d352c66af811e3bee53085a99ccac7,
  lastname: Bocker, modified: !!timestamp '2013-12-22 11:03:15', nationality: DEU,
  sex: M}
- {citizenship: true, club: 5, firstname: Carlito, guid: a7d3e9166af811e3bee53085a99ccac7,
  lastname: Bollin, modified: !!timestamp '2013-12-22 11:03:15', nationality: CHE,
  sex: M}
- {citizenship: true, club: 8, firstname: Nazrul, guid: a7d50d006af811e3bee53085a99ccac7,
  lastname: Islam, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR,
  sex: M}
- {citizenship: true, club: 8, firstname: Abdul, guid: a7d780d06af811e3bee53085a99ccac7,
  lastname: Malik, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR,
  sex: M}
- {citizenship: true, club: 10, firstname: Wolfgang, guid: a7d8a0a06af811e3bee53085a99ccac7,
  lastname: Peter, modified: !!timestamp '2013-12-22 11:03:15', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Dirk, guid: a7d93f926af811e3bee53085a99ccac7,
  lastname: Polchow, modified: !!timestamp '2013-12-22 11:03:15', nationality: DEU,
  sex: M}
- {citizenship: true, club: 8, firstname: Murshad, guid: a7dc2f686af811e3bee53085a99ccac7,
  lastname: Khan, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR, sex: M}
- {citizenship: true, club: 1, firstname: Paulina, guid: a7dcc5ea6af811e3bee53085a99ccac7,
  lastname: Nowakowska, modified: !!timestamp '2013-12-22 11:03:15', nationality: POL,
  nickname: paulina, sex: F}
- {citizenship: true, club: 9, firstname: Alfred, guid: a7de7c5a6af811e3bee53085a99ccac7,
  lastname: Rajadurai, modified: !!timestamp '2013-12-22 11:03:15', nationality: FRA,
  sex: M}
- {citizenship: true, club: 1, firstname: Sylwester, guid: a7e147b46af811e3bee53085a99ccac7,
  lastname: Pogorzelski, modified: !!timestamp '2013-12-22 11:03:15', nationality: POL,
  sex: M}
- {citizenship: true, club: 12, firstname: Pavel, guid: a7ef5e3a6af811e3bee53085a99ccac7,
  lastname: Schierl, modified: !!timestamp '2013-12-22 11:03:15', nationality: CZE,
  sex: M}
- {citizenship: true, club: 7, firstname: Carlos, guid: a7c262726af811e3bee53085a99ccac7,
  lastname: Fernandez, modified: !!timestamp '2013-12-22 11:03:15', nationality: CHE,
  sex: M}
- {citizenship: true, club: 5, firstname: Josef, guid: a807d7306af811e3bee53085a99ccac7,
  lastname: Meyer, modified: !!timestamp '2013-12-22 11:03:15', nationality: CHE,
  sex: M}
- {citizenship: true, club: 7, firstname: Nick, guid: a801969a6af811e3bee53085a99ccac7,
  lastname: Humbert, modified: !!timestamp '2013-12-22 11:03:15', nationality: DEU,
  sex: M}
- {citizenship: true, club: 8, firstname: Azir, guid: a8086d1c6af811e3bee53085a99ccac7,
  lastname: Uddin, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR,
  sex: M}
- {citizenship: true, club: 8, firstname: Ahad, guid: a80afb866af811e3bee53085a99ccac7,
  lastname: Miah, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR, sex: M}
- {citizenship: true, club: 8, firstname: Ish, guid: a80c19da6af811e3bee53085a99ccac7,
  lastname: Kumar, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR,
  sex: M}
- {citizenship: true, club: 7, firstname: Julian, guid: a80d3d106af811e3bee53085a99ccac7,
  lastname: Perkin, modified: !!timestamp '2013-12-22 11:03:15', nationality: FRA,
  sex: M}
- {citizenship: true, club: 7, firstname: Yoann, guid: a810239a6af811e3bee53085a99ccac7,
  lastname: Pidial, modified: !!timestamp '2013-12-22 11:03:15', nationality: FRA,
  sex: M}
- {citizenship: true, club: 7, firstname: Thierry, guid: a8114cf26af811e3bee53085a99ccac7,
  lastname: Huchet, modified: !!timestamp '2013-12-22 11:03:15', nationality: FRA,
  sex: M}
- {citizenship: true, club: 12, firstname: Romana, guid: a81903b66af811e3bee53085a99ccac7,
  lastname: Kralova, modified: !!timestamp '2013-12-22 11:03:15', nationality: CZE,
  sex: F}
- {citizenship: true, club: 12, firstname: Filip, guid: a8199bc86af811e3bee53085a99ccac7,
  lastname: Smoljak, modified: !!timestamp '2013-12-22 11:03:15', nationality: CZE,
  sex: M}
- {citizenship: true, club: 7, firstname: Sattirabady, guid: a81a32f46af811e3bee53085a99ccac7,
  lastname: Jupiter, modified: !!timestamp '2013-12-22 11:03:15', nationality: FRA,
  sex: M}
- {citizenship: true, club: 7, firstname: Giulio, guid: a82bacc86af811e3bee53085a99ccac7,
  lastname: Martinelli, modified: !!timestamp '2013-12-22 11:03:15', nationality: ITA,
  sex: M}
- {citizenship: true, club: 7, firstname: Mervin, guid: a7c97abc6af811e3bee53085a99ccac7,
  lastname: Kalinga, modified: !!timestamp '2013-12-22 11:03:15', nationality: CHE,
  sex: M}
- {citizenship: true, club: 7, firstname: Balendran, guid: a844b6fa6af811e3bee53085a99ccac7,
  lastname: Kanagan, modified: !!timestamp '2013-12-22 11:03:15', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Susanne, guid: a849852c6af811e3bee53085a99ccac7,
  lastname: Schackert, modified: !!timestamp '2013-12-22 11:03:15', nationality: DEU,
  sex: F}
- {citizenship: true, club: 7, firstname: Uwe, guid: a8400ee86af811e3bee53085a99ccac7,
  lastname: Schneider, modified: !!timestamp '2013-12-22 11:03:15', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Saravanan, guid: a85354126af811e3bee53085a99ccac7,
  lastname: Babu, modified: !!timestamp '2013-12-22 11:03:15', nationality: SWE, sex: M}
- {citizenship: true, club: 7, firstname: Mehedi, guid: a855bbda6af811e3bee53085a99ccac7,
  lastname: Hassan, modified: !!timestamp '2013-12-22 11:03:15', nationality: SWE,
  sex: M}
- {citizenship: true, club: 7, firstname: Amar, guid: a8577ccc6af811e3bee53085a99ccac7,
  lastname: Sanakal, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR,
  sex: M}
- {citizenship: true, club: 7, firstname: Igor, guid: a85af91a6af811e3bee53085a99ccac7,
  lastname: Khattry, modified: !!timestamp '2013-12-22 11:03:16', nationality: ESP,
  sex: M}
- {citizenship: true, club: 7, firstname: Kurt, guid: a85cc3266af811e3bee53085a99ccac7,
  lastname: Scherrer, modified: !!timestamp '2013-12-22 11:03:16', nationality: CHE,
  sex: M}
- {citizenship: true, club: 7, firstname: Barbara, guid: a85f15546af811e3bee53085a99ccac7,
  lastname: Thomas, modified: !!timestamp '2013-12-22 11:03:16', nationality: CHE,
  sex: F}
- {citizenship: true, club: 7, firstname: Kanagan, guid: a85fa47e6af811e3bee53085a99ccac7,
  lastname: Santhakumar, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 1, firstname: Robert, guid: a8986dea6af811e3bee53085a99ccac7,
  lastname: Bany, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL, sex: M}
- {citizenship: true, club: 9, firstname: Sieger, guid: a8990cfa6af811e3bee53085a99ccac7,
  lastname: Kevin, modified: !!timestamp '2013-12-22 11:03:16', nationality: FRA,
  sex: M}
- {citizenship: true, club: 12, firstname: Ondrej, guid: a899a02a6af811e3bee53085a99ccac7,
  lastname: Chmelar, modified: !!timestamp '2013-12-22 11:03:16', nationality: CZE,
  sex: M}
- {citizenship: true, club: 12, firstname: Cyrus, guid: a89bdfa26af811e3bee53085a99ccac7,
  lastname: Skaria, modified: !!timestamp '2013-12-22 11:03:16', nationality: CZE,
  sex: M}
- {citizenship: true, club: 13, firstname: Amitha, guid: a89c7b6a6af811e3bee53085a99ccac7,
  lastname: Ubhayatunga, modified: !!timestamp '2013-12-22 11:03:16', nationality: ITA,
  sex: M}
- {citizenship: true, club: 1, firstname: Nagaraj, guid: a89d11ba6af811e3bee53085a99ccac7,
  lastname: Ramakrishna, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL,
  sex: M}
- {citizenship: true, club: 10, firstname: Daniel, guid: a89da67a6af811e3bee53085a99ccac7,
  lastname: Monoharan, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 14, firstname: Gunnar, guid: a89e433c6af811e3bee53085a99ccac7,
  lastname: Von Arnold, modified: !!timestamp '2013-12-22 11:03:16', nationality: SWE,
  sex: M}
- {citizenship: true, club: 12, firstname: Tomas, guid: a8a0954c6af811e3bee53085a99ccac7,
  lastname: Chmelar, modified: !!timestamp '2013-12-22 11:03:16', nationality: CZE,
  sex: M}
- {citizenship: true, club: 8, firstname: Abdus, guid: a8a125486af811e3bee53085a99ccac7,
  lastname: Shahid, modified: !!timestamp '2013-12-22 11:03:16', nationality: GBR,
  sex: M}
- {citizenship: true, club: 1, firstname: Lukasz, guid: a8a1c7326af811e3bee53085a99ccac7,
  lastname: Kaminski, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL,
  sex: M}
- {citizenship: true, club: 9, firstname: Mourlay, guid: a8a25efe6af811e3bee53085a99ccac7,
  lastname: Venou, modified: !!timestamp '2013-12-22 11:03:16', nationality: FRA,
  sex: M}
- {citizenship: true, club: 9, firstname: Thomas, guid: a8a2f2b06af811e3bee53085a99ccac7,
  lastname: Chobe, modified: !!timestamp '2013-12-22 11:03:16', nationality: FRA,
  sex: M}
- {citizenship: true, club: 13, firstname: Lucia Elene, guid: a8a543946af811e3bee53085a99ccac7,
  lastname: Ammazzalamorte, modified: !!timestamp '2013-12-22 11:03:16', nationality: ITA,
  sex: F}
- {citizenship: true, club: 10, firstname: Abdullah, guid: a8a5d1566af811e3bee53085a99ccac7,
  lastname: Nauandish, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 13, firstname: Vanderlan, guid: a8a667566af811e3bee53085a99ccac7,
  lastname: Ayesh Nilan, modified: !!timestamp '2013-12-22 11:03:16', nationality: ITA,
  sex: M}
- {citizenship: true, club: 8, firstname: Joseph, guid: a8a705e46af811e3bee53085a99ccac7,
  lastname: Gloosberg, modified: !!timestamp '2013-12-22 11:03:16', nationality: GBR,
  sex: M}
- {citizenship: true, club: 12, firstname: Honza, guid: a8a7995a6af811e3bee53085a99ccac7,
  lastname: Tesitel, modified: !!timestamp '2013-12-22 11:03:16', nationality: CZE,
  sex: M}
- {citizenship: true, club: 10, firstname: Johanes, guid: a8a9f1d26af811e3bee53085a99ccac7,
  lastname: Jorg, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU, sex: M}
- {citizenship: true, club: 10, firstname: Paul, guid: a8aa8a3e6af811e3bee53085a99ccac7,
  lastname: Schneider, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 9, firstname: Prabagarane, guid: a8ab21a66af811e3bee53085a99ccac7,
  lastname: Deiva, modified: !!timestamp '2013-12-22 11:03:16', nationality: FRA,
  sex: M}
- {citizenship: true, club: 1, firstname: Jakub, guid: a8abb92c6af811e3bee53085a99ccac7,
  lastname: Sasinski, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL,
  sex: M}
- {citizenship: true, club: 10, firstname: Tobias, guid: a8ac64c66af811e3bee53085a99ccac7,
  lastname: Kruger, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 12, firstname: Anna, guid: a8ae8e4a6af811e3bee53085a99ccac7,
  lastname: Mazur Skaria, modified: !!timestamp '2013-12-22 11:03:16', nationality: CZE,
  sex: F}
- {citizenship: true, club: 14, firstname: Olof, guid: a8af19a06af811e3bee53085a99ccac7,
  lastname: Klint, modified: !!timestamp '2013-12-22 11:03:16', nationality: SWE,
  sex: M}
- {citizenship: true, club: 8, firstname: Chandan, guid: a8afbbe46af811e3bee53085a99ccac7,
  lastname: Narkar, modified: !!timestamp '2013-12-22 11:03:16', nationality: GBR,
  sex: M}
- {citizenship: true, club: 1, firstname: Ajay, guid: a8b051626af811e3bee53085a99ccac7,
  lastname: Pal, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL, sex: M}
- {citizenship: true, club: 10, firstname: Eshan, guid: a8b0e67c6af811e3bee53085a99ccac7,
  lastname: Mannan, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 8, firstname: Rahat, guid: a8b34c826af811e3bee53085a99ccac7,
  lastname: Islam, modified: !!timestamp '2013-12-22 11:03:16', nationality: GBR,
  sex: M}
- {citizenship: true, club: 1, firstname: Bartosz, guid: a8b3dca66af811e3bee53085a99ccac7,
  lastname: Sasinski, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL,
  sex: M}
- {citizenship: true, club: 15, firstname: Luis Felipe, guid: a8b4721a6af811e3bee53085a99ccac7,
  lastname: Mendez Banderas, modified: !!timestamp '2013-12-22 11:03:16', nationality: ESP,
  sex: M}
- {citizenship: true, club: 9, firstname: Thierry, guid: a8b514546af811e3bee53085a99ccac7,
  lastname: Berthelemy, modified: !!timestamp '2013-12-22 11:03:16', nationality: FRA,
  sex: M}
- {citizenship: true, club: 8, firstname: Misbah, guid: a8b5a98c6af811e3bee53085a99ccac7,
  lastname: Ahmed, modified: !!timestamp '2013-12-22 11:03:16', nationality: GBR,
  sex: M}
- {citizenship: true, club: 1, firstname: Wojtek, guid: a8b7e9e06af811e3bee53085a99ccac7,
  lastname: Rzewuski, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL,
  sex: M}
- {citizenship: true, club: 10, firstname: Soundararajah, guid: a8b884866af811e3bee53085a99ccac7,
  lastname: Rangasamy, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 10, firstname: Clement, guid: a8b918ec6af811e3bee53085a99ccac7,
  lastname: Anthony, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 8, firstname: Harvery, guid: a8b9ae4c6af811e3bee53085a99ccac7,
  lastname: Gomes, modified: !!timestamp '2013-12-22 11:03:16', nationality: GBR,
  sex: M}
- {citizenship: true, club: 1, firstname: Rafal, guid: a8ba4fb46af811e3bee53085a99ccac7,
  lastname: Szczepanik, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL,
  sex: M}
- {citizenship: true, club: 10, firstname: Mark, guid: a8bc997c6af811e3bee53085a99ccac7,
  lastname: Muller -Linow, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
rates: []
ratings: []
championships:
- {closed: false, club: 1, couplings: serial, description: Single Event, guid: a931a0d26af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:17', playersperteam: 1,
  prizes: millesimal, skipworstprizes: 0}
---
competitors:
- bucholz: 126
  netscore: -67
  players: [1]
  points: 10
  totscore: 127
- bucholz: 142
  netscore: 75
  players: [2]
  points: 14
  totscore: 206
- bucholz: 99
  netscore: -8
  players: [3]
  points: 10
  totscore: 154
- bucholz: 141
  netscore: 60
  players: [4]
  points: 14
  totscore: 207
- bucholz: 137
  netscore: -1
  players: [5]
  points: 12
  totscore: 168
- bucholz: 120
  netscore: -26
  players: [6]
  points: 10
  totscore: 155
- bucholz: 144
  netscore: 10
  players: [7]
  points: 14
  totscore: 197
- bucholz: 118
  netscore: -25
  players: [8]
  points: 11
  totscore: 151
- bucholz: 133
  netscore: 77
  players: [9]
  points: 13
  totscore: 206
- bucholz: 131
  netscore: 27
  players: [10]
  points: 12
  totscore: 183
- bucholz: 107
  netscore: -103
  players: [11]
  points: 8
  totscore: 114
- bucholz: 125
  netscore: 29
  players: [12]
  points: 13
  totscore: 192
- bucholz: 109
  netscore: 10
  players: [13]
  points: 12
  totscore: 168
- bucholz: 112
  netscore: -63
  players: [14]
  points: 6
  totscore: 132
- bucholz: 106
  netscore: 27
  players: [15]
  points: 10
  totscore: 172
- bucholz: 120
  netscore: 2
  players: [16]
  points: 12
  totscore: 180
- bucholz: 129
  netscore: 75
  players: [17]
  points: 14
  totscore: 217
- bucholz: 127
  netscore: 5
  players: [18]
  points: 12
  totscore: 168
- bucholz: 132
  netscore: 40
  players: [19]
  points: 12
  totscore: 210
- bucholz: 124
  netscore: -71
  players: [20]
  points: 8
  totscore: 148
- bucholz: 108
  netscore: -9
  players: [21]
  points: 12
  totscore: 160
- bucholz: 169
  netscore: 50
  players: [22]
  points: 14
  totscore: 199
- bucholz: 171
  netscore: 106
  players: [23]
  points: 17
  totscore: 235
- bucholz: 105
  netscore: -32
  players: [24]
  points: 9
  totscore: 167
- bucholz: 115
  netscore: -14
  players: [25]
  points: 11
  totscore: 160
- bucholz: 104
  netscore: 3
  players: [26]
  points: 10
  totscore: 164
- bucholz: 171
  netscore: 110
  players: [27]
  points: 16
  totscore: 224
- bucholz: 100
  netscore: -56
  players: [28]
  points: 8
  totscore: 141
- bucholz: 126
  netscore: 17
  players: [29]
  points: 10
  totscore: 181
- bucholz: 131
  netscore: 36
  players: [30]
  points: 14
  totscore: 192
- bucholz: 113
  netscore: 34
  players: [31]
  points: 12
  totscore: 183
- bucholz: 98
  netscore: 23
  players: [32]
  points: 10
  totscore: 196
- bucholz: 157
  netscore: 80
  players: [33]
  points: 16
  totscore: 224
- bucholz: 145
  netscore: 117
  players: [34]
  points: 16
  totscore: 228
- bucholz: 170
  netscore: 142
  players: [35]
  points: 20
  totscore: 258
- bucholz: 139
  netscore: 16
  players: [36]
  points: 12
  totscore: 176
- bucholz: 154
  netscore: 84
  players: [37]
  points: 17
  totscore: 213
- bucholz: 152
  netscore: 25
  players: [38]
  points: 14
  totscore: 188
- bucholz: 149
  netscore: 76
  players: [39]
  points: 15
  totscore: 220
- bucholz: 137
  netscore: 81
  players: [40]
  points: 13
  totscore: 217
- bucholz: 134
  netscore: 64
  players: [41]
  points: 14
  totscore: 203
- bucholz: 127
  netscore: 96
  players: [42]
  points: 14
  totscore: 224
- bucholz: 144
  netscore: 134
  players: [43]
  points: 18
  totscore: 236
- bucholz: 122
  netscore: 4
  players: [44]
  points: 10
  totscore: 172
- bucholz: 145
  netscore: 3
  players: [45]
  points: 12
  totscore: 182
- bucholz: 140
  netscore: -31
  players: [46]
  points: 12
  totscore: 169
- bucholz: 71
  netscore: -13
  players: [47]
  points: 8
  totscore: 135
- bucholz: 123
  netscore: 14
  players: [48]
  points: 10
  totscore: 183
- bucholz: 125
  netscore: 40
  players: [49]
  points: 12
  totscore: 195
- bucholz: 125
  netscore: -56
  players: [50]
  points: 11
  totscore: 128
- bucholz: 86
  netscore: -68
  players: [51]
  points: 8
  totscore: 116
- bucholz: 130
  netscore: -29
  players: [52]
  points: 10
  totscore: 161
- bucholz: 149
  netscore: 54
  players: [53]
  points: 14
  totscore: 190
- bucholz: 102
  netscore: -32
  players: [54]
  points: 8
  totscore: 126
- bucholz: 113
  netscore: 12
  players: [55]
  points: 10
  totscore: 171
- bucholz: 148
  netscore: 41
  players: [56]
  points: 14
  totscore: 199
- bucholz: 92
  netscore: -24
  players: [57]
  points: 8
  totscore: 153
- bucholz: 126
  netscore: -36
  players: [58]
  points: 10
  totscore: 136
- bucholz: 116
  netscore: -75
  players: [59]
  points: 8
  totscore: 131
- bucholz: 96
  netscore: -8
  players: [60]
  points: 9
  totscore: 153
- bucholz: 133
  netscore: 58
  players: [61]
  points: 14
  totscore: 203
- bucholz: 136
  netscore: 71
  players: [62]
  points: 13
  totscore: 197
- bucholz: 119
  netscore: -30
  players: [63]
  points: 10
  totscore: 156
- bucholz: 123
  netscore: -28
  players: [64]
  points: 12
  totscore: 152
- bucholz: 133
  netscore: 100
  players: [65]
  points: 14
  totscore: 211
- bucholz: 154
  netscore: 134
  players: [66]
  points: 16
  totscore: 229
- bucholz: 131
  netscore: 57
  players: [67]
  points: 14
  totscore: 201
- bucholz: 73
  netscore: -87
  players: [68]
  points: 6
  totscore: 100
- bucholz: 110
  netscore: -13
  players: [69]
  points: 10
  totscore: 167
- bucholz: 88
  netscore: -50
  players: [70]
  points: 8
  totscore: 123
- bucholz: 109
  netscore: -31
  players: [71]
  points: 9
  totscore: 139
- bucholz: 108
  netscore: -37
  players: [72]
  points: 10
  totscore: 139
- bucholz: 143
  netscore: -35
  players: [73]
  points: 13
  totscore: 157
- bucholz: 114
  netscore: 28
  players: [74]
  points: 10
  totscore: 172
- bucholz: 102
  netscore: -15
  players: [75]
  points: 8
  totscore: 180
- bucholz: 134
  netscore: 54
  players: [76]
  points: 12
  totscore: 195
- bucholz: 102
  netscore: -23
  players: [77]
  points: 10
  totscore: 141
- bucholz: 117
  netscore: -13
  players: [78]
  points: 10
  totscore: 163
- bucholz: 91
  netscore: -98
  players: [79]
  points: 6
  totscore: 117
- bucholz: 93
  netscore: -76
  players: [80]
  points: 6
  totscore: 99
- bucholz: 102
  netscore: -8
  players: [81]
  points: 10
  totscore: 177
- bucholz: 90
  netscore: -141
  players: [82]
  points: 6
  totscore: 78
- bucholz: 150
  netscore: 86
  players: [83]
  points: 16
  totscore: 214
- bucholz: 127
  netscore: 6
  players: [84]
  points: 12
  totscore: 160
- bucholz: 86
  netscore: -154
  players: [85]
  points: 4
  totscore: 78
- bucholz: 118
  netscore: -12
  players: [86]
  points: 10
  totscore: 158
- bucholz: 131
  netscore: 8
  players: [87]
  points: 11
  totscore: 171
- bucholz: 94
  netscore: -118
  players: [88]
  points: 6
  totscore: 68
- bucholz: 111
  netscore: -86
  players: [89]
  points: 8
  totscore: 116
- bucholz: 161
  netscore: 41
  players: [90]
  points: 15
  totscore: 198
- bucholz: 72
  netscore: -31
  players: [91]
  points: 8
  totscore: 129
- bucholz: 101
  netscore: -34
  players: [92]
  points: 8
  totscore: 159
- bucholz: 120
  netscore: -56
  players: [93]
  points: 10
  totscore: 146
- bucholz: 148
  netscore: 64
  players: [94]
  points: 14
  totscore: 206
- bucholz: 70
  netscore: -120
  players: [95]
  points: 4
  totscore: 68
- bucholz: 59
  netscore: -164
  players: [96]
  retired: true
  totscore: 6
- bucholz: 147
  netscore: 79
  players: [97]
  points: 15
  totscore: 210
- bucholz: 106
  netscore: 8
  players: [98]
  points: 12
  totscore: 166
- bucholz: 130
  netscore: -6
  players: [99]
  points: 12
  totscore: 152
- bucholz: 108
  netscore: -39
  players: [100]
  points: 8
  totscore: 159
- bucholz: 110
  netscore: -57
  players: [101]
  points: 8
  totscore: 142
- bucholz: 93
  netscore: -132
  players: [102]
  points: 6
  totscore: 71
- bucholz: 69
  netscore: -48
  players: [103]
  points: 6
  totscore: 94
- bucholz: 141
  netscore: -7
  players: [104]
  points: 11
  totscore: 172
- bucholz: 116
  netscore: -68
  players: [105]
  points: 10
  totscore: 127
- bucholz: 126
  netscore: 39
  players: [106]
  points: 12
  totscore: 184
- bucholz: 131
  netscore: -6
  players: [107]
  points: 12
  totscore: 179
- bucholz: 107
  netscore: -59
  players: [108]
  points: 8
  totscore: 136
- bucholz: 105
  netscore: -46
  players: [109]
  points: 8
  totscore: 138
- bucholz: 142
  netscore: 118
  players: [110]
  points: 16
  totscore: 220
couplings: dazed
currentturn: 11
date: 2013-06-22
description: 17th Carrom Euro Cup - Single
duration: 55
guid: b45c38786af811e3bee53085a99ccac7
location: Leszno
matches:
- {board: 1, competitor1: 35, competitor2: 62, score1: 25, score2: 0, turn: 1}
- {board: 2, competitor1: 27, competitor2: 18, score1: 25, score2: 0, turn: 1}
- {board: 3, competitor1: 37, competitor2: 49, score1: 20, score2: 15, turn: 1}
- {board: 4, competitor1: 94, competitor2: 69, score1: 25, score2: 5, turn: 1}
- {board: 5, competitor1: 23, competitor2: 4, score1: 25, score2: 17, turn: 1}
- {board: 6, competitor1: 83, competitor2: 46, score1: 25, score2: 7, turn: 1}
- {board: 7, competitor1: 39, competitor2: 86, score1: 25, score2: 0, turn: 1}
- {board: 8, competitor1: 38, competitor2: 25, score1: 25, score2: 10, turn: 1}
- {board: 9, competitor1: 2, competitor2: 40, score1: 25, score2: 14, turn: 1}
- {board: 10, competitor1: 33, competitor2: 48, score1: 25, score2: 13, turn: 1}
- {board: 11, competitor1: 97, competitor2: 44, score1: 25, score2: 7, turn: 1}
- {board: 12, competitor1: 42, competitor2: 64, score1: 15, score2: 20, turn: 1}
- {board: 13, competitor1: 66, competitor2: 36, score1: 25, score2: 0, turn: 1}
- {board: 14, competitor1: 22, competitor2: 6, score1: 25, score2: 4, turn: 1}
- {board: 15, competitor1: 81, competitor2: 8, score1: 18, score2: 18, turn: 1}
- {board: 16, competitor1: 110, competitor2: 74, score1: 25, score2: 0, turn: 1}
- {board: 17, competitor1: 90, competitor2: 67, score1: 16, score2: 11, turn: 1}
- {board: 18, competitor1: 65, competitor2: 73, score1: 9, score2: 16, turn: 1}
- {board: 19, competitor1: 76, competitor2: 103, score1: 25, score2: 0, turn: 1}
- {board: 20, competitor1: 7, competitor2: 63, score1: 24, score2: 20, turn: 1}
- {board: 21, competitor1: 17, competitor2: 87, score1: 11, score2: 20, turn: 1}
- {board: 22, competitor1: 9, competitor2: 10, score1: 24, score2: 9, turn: 1}
- {board: 23, competitor1: 61, competitor2: 104, score1: 16, score2: 23, turn: 1}
- {board: 24, competitor1: 41, competitor2: 12, score1: 21, score2: 12, turn: 1}
- {board: 25, competitor1: 45, competitor2: 72, score1: 25, score2: 7, turn: 1}
- {board: 26, competitor1: 106, competitor2: 31, score1: 25, score2: 6, turn: 1}
- {board: 27, competitor1: 5, competitor2: 26, score1: 25, score2: 6, turn: 1}
- {board: 28, competitor1: 15, competitor2: 56, score1: 11, score2: 22, turn: 1}
- {board: 29, competitor1: 34, competitor2: 52, score1: 25, score2: 2, turn: 1}
- {board: 30, competitor1: 53, competitor2: 30, score1: 25, score2: 1, turn: 1}
- {board: 31, competitor1: 29, competitor2: 32, score1: 24, score2: 13, turn: 1}
- {board: 32, competitor1: 55, competitor2: 13, score1: 15, score2: 9, turn: 1}
- {board: 33, competitor1: 1, competitor2: 57, score1: 21, score2: 6, turn: 1}
- {board: 34, competitor1: 20, competitor2: 85, score1: 25, score2: 0, turn: 1}
- {board: 35, competitor1: 79, competitor2: 24, score1: 5, score2: 25, turn: 1}
- {board: 36, competitor1: 51, competitor2: 89, score1: 7, score2: 21, turn: 1}
- {board: 37, competitor1: 47, competitor2: 109, score1: 0, score2: 25, turn: 1}
- {board: 38, competitor1: 21, competitor2: 75, score1: 16, score2: 23, turn: 1}
- {board: 39, competitor1: 54, competitor2: 91, score1: 19, score2: 8, turn: 1}
- {board: 40, competitor1: 11, competitor2: 101, score1: 11, score2: 25, turn: 1}
- {board: 41, competitor1: 84, competitor2: 93, score1: 25, score2: 0, turn: 1}
- {board: 42, competitor1: 3, competitor2: 71, score1: 25, score2: 8, turn: 1}
- {board: 43, competitor1: 14, competitor2: 77, score1: 25, score2: 0, turn: 1}
- {board: 44, competitor1: 88, competitor2: 105, score1: 4, score2: 13, turn: 1}
- {board: 45, competitor1: 16, competitor2: 98, score1: 25, score2: 4, turn: 1}
- {board: 46, competitor1: 58, competitor2: 78, score1: 7, score2: 10, turn: 1}
- {board: 47, competitor1: 108, competitor2: 102, score1: 25, score2: 5, turn: 1}
- {board: 48, competitor1: 59, competitor2: 95, score1: 25, score2: 5, turn: 1}
- {board: 49, competitor1: 68, competitor2: 99, score1: 0, score2: 25, turn: 1}
- {board: 50, competitor1: 60, competitor2: 96, score1: 12, score2: 0, turn: 1}
- {board: 51, competitor1: 70, competitor2: 19, score1: 0, score2: 25, turn: 1}
- {board: 52, competitor1: 107, competitor2: 82, score1: 25, score2: 0, turn: 1}
- {board: 53, competitor1: 50, competitor2: 100, score1: 25, score2: 7, turn: 1}
- {board: 54, competitor1: 43, competitor2: 28, score1: 25, score2: 0, turn: 1}
- {board: 55, competitor1: 80, competitor2: 92, score1: 12, score2: 15, turn: 1}
- {board: 1, competitor1: 76, competitor2: 3, score1: 25, score2: 0, turn: 2}
- {board: 2, competitor1: 14, competitor2: 38, score1: 4, score2: 25, turn: 2}
- {board: 3, competitor1: 99, competitor2: 9, score1: 0, score2: 25, turn: 2}
- {board: 4, competitor1: 107, competitor2: 1, score1: 25, score2: 15, turn: 2}
- {board: 5, competitor1: 110, competitor2: 101, score1: 25, score2: 9, turn: 2}
- {board: 6, competitor1: 66, competitor2: 89, score1: 25, score2: 0, turn: 2}
- {board: 7, competitor1: 27, competitor2: 33, score1: 25, score2: 5, turn: 2}
- {board: 8, competitor1: 84, competitor2: 60, score1: 25, score2: 8, turn: 2}
- {board: 9, competitor1: 39, competitor2: 29, score1: 25, score2: 4, turn: 2}
- {board: 10, competitor1: 35, competitor2: 2, score1: 25, score2: 0, turn: 2}
- {board: 11, competitor1: 19, competitor2: 56, score1: 7, score2: 22, turn: 2}
- {board: 12, competitor1: 43, competitor2: 54, score1: 25, score2: 4, turn: 2}
- {board: 13, competitor1: 20, competitor2: 41, score1: 18, score2: 12, turn: 2}
- {board: 14, competitor1: 109, competitor2: 87, score1: 3, score2: 25, turn: 2}
- {board: 15, competitor1: 53, competitor2: 105, score1: 25, score2: 5, turn: 2}
- {board: 16, competitor1: 34, competitor2: 23, score1: 11, score2: 14, turn: 2}
- {board: 17, competitor1: 16, competitor2: 75, score1: 22, score2: 14, turn: 2}
- {board: 18, competitor1: 22, competitor2: 104, score1: 24, score2: 9, turn: 2}
- {board: 19, competitor1: 94, competitor2: 73, score1: 25, score2: 1, turn: 2}
- {board: 20, competitor1: 24, competitor2: 55, score1: 18, score2: 12, turn: 2}
- {board: 21, competitor1: 59, competitor2: 64, score1: 25, score2: 3, turn: 2}
- {board: 22, competitor1: 108, competitor2: 37, score1: 6, score2: 25, turn: 2}
- {board: 23, competitor1: 5, competitor2: 90, score1: 15, score2: 25, turn: 2}
- {board: 24, competitor1: 106, competitor2: 7, score1: 5, score2: 23, turn: 2}
- {board: 25, competitor1: 50, competitor2: 92, score1: 21, score2: 11, turn: 2}
- {board: 26, competitor1: 83, competitor2: 78, score1: 25, score2: 2, turn: 2}
- {board: 27, competitor1: 45, competitor2: 97, score1: 10, score2: 25, turn: 2}
- {board: 28, competitor1: 8, competitor2: 80, score1: 21, score2: 8, turn: 2}
- {board: 29, competitor1: 81, competitor2: 58, score1: 18, score2: 19, turn: 2}
- {board: 30, competitor1: 63, competitor2: 72, score1: 15, score2: 9, turn: 2}
- {board: 31, competitor1: 42, competitor2: 26, score1: 25, score2: 1, turn: 2}
- {board: 32, competitor1: 49, competitor2: 31, score1: 11, score2: 25, turn: 2}
- {board: 33, competitor1: 67, competitor2: 95, score1: 25, score2: 0, turn: 2}
- {board: 34, competitor1: 13, competitor2: 102, score1: 16, score2: 9, turn: 2}
- {board: 35, competitor1: 61, competitor2: 69, score1: 25, score2: 6, turn: 2}
- {board: 36, competitor1: 21, competitor2: 79, score1: 21, score2: 3, turn: 2}
- {board: 37, competitor1: 65, competitor2: 6, score1: 25, score2: 0, turn: 2}
- {board: 38, competitor1: 4, competitor2: 98, score1: 25, score2: 1, turn: 2}
- {board: 39, competitor1: 12, competitor2: 52, score1: 23, score2: 18, turn: 2}
- {board: 40, competitor1: 17, competitor2: 30, score1: 22, score2: 13, turn: 2}
- {board: 41, competitor1: 88, competitor2: 74, score1: 4, score2: 25, turn: 2}
- {board: 42, competitor1: 40, competitor2: 47, score1: 25, score2: 0, turn: 2}
- {board: 43, competitor1: 32, competitor2: 85, score1: 25, score2: 0, turn: 2}
- {board: 44, competitor1: 15, competitor2: 18, score1: 9, score2: 25, turn: 2}
- {board: 45, competitor1: 91, competitor2: 86, score1: 7, score2: 18, turn: 2}
- {board: 46, competitor1: 48, competitor2: 62, score1: 14, score2: 25, turn: 2}
- {board: 47, competitor1: 96, competitor2: 68, score1: 0, score2: 12, turn: 2}
- {board: 48, competitor1: 11, competitor2: 36, score1: 4, score2: 25, turn: 2}
- {board: 49, competitor1: 51, competitor2: 70, score1: 25, score2: 11, turn: 2}
- {board: 50, competitor1: 25, competitor2: 103, score1: 25, score2: 0, turn: 2}
- {board: 51, competitor1: 10, competitor2: 28, score1: 25, score2: 0, turn: 2}
- {board: 52, competitor1: 57, competitor2: 93, score1: 16, score2: 22, turn: 2}
- {board: 53, competitor1: 71, competitor2: 77, score1: 11, score2: 10, turn: 2}
- {board: 54, competitor1: 44, competitor2: 82, score1: 25, score2: 0, turn: 2}
- {board: 55, competitor1: 100, competitor2: 46, score1: 16, score2: 23, turn: 2}
- {board: 1, competitor1: 39, competitor2: 94, score1: 25, score2: 9, turn: 3}
- {board: 2, competitor1: 66, competitor2: 76, score1: 25, score2: 15, turn: 3}
- {board: 3, competitor1: 35, competitor2: 43, score1: 25, score2: 5, turn: 3}
- {board: 4, competitor1: 27, competitor2: 53, score1: 25, score2: 4, turn: 3}
- {board: 5, competitor1: 84, competitor2: 59, score1: 25, score2: 2, turn: 3}
- {board: 6, competitor1: 110, competitor2: 22, score1: 3, score2: 25, turn: 3}
- {board: 7, competitor1: 83, competitor2: 107, score1: 25, score2: 7, turn: 3}
- {board: 8, competitor1: 9, competitor2: 20, score1: 21, score2: 23, turn: 3}
- {board: 9, competitor1: 38, competitor2: 16, score1: 25, score2: 1, turn: 3}
- {board: 10, competitor1: 97, competitor2: 50, score1: 16, score2: 16, turn: 3}
- {board: 11, competitor1: 7, competitor2: 24, score1: 25, score2: 7, turn: 3}
- {board: 12, competitor1: 87, competitor2: 56, score1: 6, score2: 19, turn: 3}
- {board: 13, competitor1: 90, competitor2: 23, score1: 24, score2: 24, turn: 3}
- {board: 14, competitor1: 37, competitor2: 8, score1: 25, score2: 3, turn: 3}
- {board: 15, competitor1: 41, competitor2: 101, score1: 22, score2: 6, turn: 3}
- {board: 16, competitor1: 55, competitor2: 36, score1: 0, score2: 25, turn: 3}
- {board: 17, competitor1: 106, competitor2: 74, score1: 21, score2: 18, turn: 3}
- {board: 18, competitor1: 99, competitor2: 60, score1: 20, score2: 5, turn: 3}
- {board: 19, competitor1: 75, competitor2: 92, score1: 25, score2: 21, turn: 3}
- {board: 20, competitor1: 104, competitor2: 33, score1: 13, score2: 19, turn: 3}
- {board: 21, competitor1: 3, competitor2: 18, score1: 12, score2: 21, turn: 3}
- {board: 22, competitor1: 29, competitor2: 54, score1: 25, score2: 4, turn: 3}
- {board: 23, competitor1: 89, competitor2: 46, score1: 8, score2: 21, turn: 3}
- {board: 24, competitor1: 2, competitor2: 105, score1: 25, score2: 7, turn: 3}
- {board: 25, competitor1: 64, competitor2: 62, score1: 1, score2: 25, turn: 3}
- {board: 26, competitor1: 73, competitor2: 86, score1: 25, score2: 16, turn: 3}
- {board: 27, competitor1: 78, competitor2: 93, score1: 17, score2: 18, turn: 3}
- {board: 28, competitor1: 67, competitor2: 58, score1: 25, score2: 11, turn: 3}
- {board: 29, competitor1: 4, competitor2: 65, score1: 22, score2: 15, turn: 3}
- {board: 30, competitor1: 34, competitor2: 42, score1: 14, score2: 21, turn: 3}
- {board: 31, competitor1: 25, competitor2: 40, score1: 0, score2: 25, turn: 3}
- {board: 32, competitor1: 10, competitor2: 32, score1: 25, score2: 9, turn: 3}
- {board: 33, competitor1: 19, competitor2: 61, score1: 22, score2: 25, turn: 3}
- {board: 34, competitor1: 5, competitor2: 21, score1: 25, score2: 0, turn: 3}
- {board: 35, competitor1: 44, competitor2: 13, score1: 7, score2: 23, turn: 3}
- {board: 36, competitor1: 14, competitor2: 12, score1: 10, score2: 25, turn: 3}
- {board: 37, competitor1: 1, competitor2: 51, score1: 12, score2: 0, turn: 3}
- {board: 38, competitor1: 45, competitor2: 31, score1: 25, score2: 0, turn: 3}
- {board: 39, competitor1: 109, competitor2: 68, score1: 25, score2: 8, turn: 3}
- {board: 40, competitor1: 63, competitor2: 71, score1: 14, score2: 13, turn: 3}
- {board: 41, competitor1: 108, competitor2: 17, score1: 8, score2: 22, turn: 3}
- {board: 42, competitor1: 81, competitor2: 49, score1: 12, score2: 25, turn: 3}
- {board: 43, competitor1: 100, competitor2: 91, score1: 25, score2: 0, turn: 3}
- {board: 44, competitor1: 30, competitor2: 72, score1: 25, score2: 7, turn: 3}
- {board: 45, competitor1: 15, competitor2: 48, score1: 23, score2: 6, turn: 3}
- {board: 46, competitor1: 79, competitor2: 96, score1: 21, score2: 6, turn: 3}
- {board: 47, competitor1: 69, competitor2: 77, score1: 21, score2: 12, turn: 3}
- {board: 48, competitor1: 98, competitor2: 52, score1: 7, score2: 15, turn: 3}
- {board: 49, competitor1: 95, competitor2: 102, score1: 2, score2: 15, turn: 3}
- {board: 50, competitor1: 6, competitor2: 88, score1: 25, score2: 0, turn: 3}
- {board: 51, competitor1: 85, competitor2: 11, score1: 6, score2: 25, turn: 3}
- {board: 52, competitor1: 103, competitor2: 70, score1: 5, score2: 14, turn: 3}
- {board: 53, competitor1: 28, competitor2: 26, score1: 0, score2: 25, turn: 3}
- {board: 54, competitor1: 80, competitor2: 57, score1: 9, score2: 17, turn: 3}
- {board: 55, competitor1: 82, competitor2: 47, score1: 18, score2: 16, turn: 3}
- {board: 1, competitor1: 27, competitor2: 22, score1: 25, score2: 0, turn: 4}
- {board: 2, competitor1: 7, competitor2: 56, score1: 21, score2: 18, turn: 4}
- {board: 3, competitor1: 84, competitor2: 20, score1: 20, score2: 10, turn: 4}
- {board: 4, competitor1: 35, competitor2: 38, score1: 25, score2: 5, turn: 4}
- {board: 5, competitor1: 39, competitor2: 37, score1: 20, score2: 20, turn: 4}
- {board: 6, competitor1: 90, competitor2: 50, score1: 25, score2: 0, turn: 4}
- {board: 7, competitor1: 97, competitor2: 23, score1: 9, score2: 25, turn: 4}
- {board: 8, competitor1: 66, competitor2: 83, score1: 25, score2: 5, turn: 4}
- {board: 9, competitor1: 9, competitor2: 1, score1: 25, score2: 0, turn: 4}
- {board: 10, competitor1: 87, competitor2: 99, score1: 12, score2: 16, turn: 4}
- {board: 11, competitor1: 94, competitor2: 17, score1: 24, score2: 13, turn: 4}
- {board: 12, competitor1: 41, competitor2: 62, score1: 19, score2: 12, turn: 4}
- {board: 13, competitor1: 107, competitor2: 12, score1: 13, score2: 25, turn: 4}
- {board: 14, competitor1: 2, competitor2: 75, score1: 25, score2: 2, turn: 4}
- {board: 15, competitor1: 36, competitor2: 33, score1: 0, score2: 25, turn: 4}
- {board: 16, competitor1: 53, competitor2: 63, score1: 25, score2: 6, turn: 4}
- {board: 17, competitor1: 110, competitor2: 73, score1: 25, score2: 0, turn: 4}
- {board: 18, competitor1: 29, competitor2: 67, score1: 15, score2: 16, turn: 4}
- {board: 19, competitor1: 24, competitor2: 45, score1: 5, score2: 25, turn: 4}
- {board: 20, competitor1: 16, competitor2: 4, score1: 14, score2: 25, turn: 4}
- {board: 21, competitor1: 106, competitor2: 40, score1: 3, score2: 25, turn: 4}
- {board: 22, competitor1: 46, competitor2: 42, score1: 25, score2: 16, turn: 4}
- {board: 23, competitor1: 18, competitor2: 10, score1: 11, score2: 14, turn: 4}
- {board: 24, competitor1: 93, competitor2: 109, score1: 24, score2: 5, turn: 4}
- {board: 25, competitor1: 5, competitor2: 13, score1: 20, score2: 12, turn: 4}
- {board: 26, competitor1: 76, competitor2: 61, score1: 25, score2: 3, turn: 4}
- {board: 27, competitor1: 43, competitor2: 59, score1: 25, score2: 0, turn: 4}
- {board: 28, competitor1: 19, competitor2: 32, score1: 25, score2: 7, turn: 4}
- {board: 29, competitor1: 8, competitor2: 104, score1: 13, score2: 23, turn: 4}
- {board: 30, competitor1: 108, competitor2: 74, score1: 2, score2: 24, turn: 4}
- {board: 31, competitor1: 78, competitor2: 51, score1: 25, score2: 0, turn: 4}
- {board: 32, competitor1: 89, competitor2: 57, score1: 19, score2: 18, turn: 4}
- {board: 33, competitor1: 55, competitor2: 30, score1: 13, score2: 25, turn: 4}
- {board: 34, competitor1: 64, competitor2: 26, score1: 14, score2: 11, turn: 4}
- {board: 35, competitor1: 34, competitor2: 6, score1: 23, score2: 9, turn: 4}
- {board: 36, competitor1: 44, competitor2: 105, score1: 25, score2: 0, turn: 4}
- {board: 37, competitor1: 15, competitor2: 68, score1: 23, score2: 5, turn: 4}
- {board: 38, competitor1: 65, competitor2: 69, score1: 25, score2: 4, turn: 4}
- {board: 39, competitor1: 21, competitor2: 58, score1: 4, score2: 17, turn: 4}
- {board: 40, competitor1: 14, competitor2: 54, score1: 10, score2: 9, turn: 4}
- {board: 41, competitor1: 25, competitor2: 102, score1: 22, score2: 4, turn: 4}
- {board: 42, competitor1: 60, competitor2: 52, score1: 4, score2: 25, turn: 4}
- {board: 43, competitor1: 3, competitor2: 11, score1: 9, score2: 15, turn: 4}
- {board: 44, competitor1: 101, competitor2: 71, score1: 14, score2: 14, turn: 4}
- {board: 45, competitor1: 31, competitor2: 82, score1: 25, score2: 1, turn: 4}
- {board: 46, competitor1: 100, competitor2: 70, score1: 17, score2: 14, turn: 4}
- {board: 47, competitor1: 86, competitor2: 79, score1: 25, score2: 9, turn: 4}
- {board: 48, competitor1: 49, competitor2: 92, score1: 25, score2: 0, turn: 4}
- {board: 49, competitor1: 81, competitor2: 48, score1: 8, score2: 25, turn: 4}
- {board: 50, competitor1: 72, competitor2: 80, score1: 25, score2: 5, turn: 4}
- {board: 51, competitor1: 47, competitor2: 77, score1: 5, score2: 25, turn: 4}
- {board: 52, competitor1: 98, competitor2: 96, score1: 25, score2: 0, turn: 4}
- {board: 53, competitor1: 95, competitor2: 91, score1: 0, score2: 20, turn: 4}
- {board: 54, competitor1: 85, competitor2: 88, score1: 0, score2: 25, turn: 4}
- {board: 55, competitor1: 28, competitor2: 103, score1: 25, score2: 0, turn: 4}
- {board: 1, competitor1: 7, competitor2: 90, score1: 0, score2: 25, turn: 5}
- {board: 2, competitor1: 23, competitor2: 39, score1: 25, score2: 7, turn: 5}
- {board: 3, competitor1: 83, competitor2: 110, score1: 9, score2: 24, turn: 5}
- {board: 4, competitor1: 37, competitor2: 9, score1: 24, score2: 2, turn: 5}
- {board: 5, competitor1: 35, competitor2: 66, score1: 25, score2: 5, turn: 5}
- {board: 6, competitor1: 27, competitor2: 84, score1: 25, score2: 6, turn: 5}
- {board: 7, competitor1: 38, competitor2: 94, score1: 25, score2: 6, turn: 5}
- {board: 8, competitor1: 56, competitor2: 4, score1: 25, score2: 6, turn: 5}
- {board: 9, competitor1: 22, competitor2: 43, score1: 22, score2: 12, turn: 5}
- {board: 10, competitor1: 20, competitor2: 5, score1: 8, score2: 25, turn: 5}
- {board: 11, competitor1: 2, competitor2: 45, score1: 15, score2: 17, turn: 5}
- {board: 12, competitor1: 53, competitor2: 76, score1: 25, score2: 6, turn: 5}
- {board: 13, competitor1: 33, competitor2: 10, score1: 24, score2: 7, turn: 5}
- {board: 14, competitor1: 41, competitor2: 67, score1: 6, score2: 25, turn: 5}
- {board: 15, competitor1: 12, competitor2: 40, score1: 8, score2: 25, turn: 5}
- {board: 16, competitor1: 46, competitor2: 93, score1: 24, score2: 21, turn: 5}
- {board: 17, competitor1: 99, competitor2: 97, score1: 2, score2: 24, turn: 5}
- {board: 18, competitor1: 50, competitor2: 106, score1: 6, score2: 25, turn: 5}
- {board: 19, competitor1: 87, competitor2: 100, score1: 23, score2: 8, turn: 5}
- {board: 20, competitor1: 62, competitor2: 86, score1: 25, score2: 0, turn: 5}
- {board: 21, competitor1: 36, competitor2: 19, score1: 20, score2: 7, turn: 5}
- {board: 22, competitor1: 18, competitor2: 65, score1: 0, score2: 25, turn: 5}
- {board: 23, competitor1: 73, competitor2: 49, score1: 22, score2: 6, turn: 5}
- {board: 24, competitor1: 104, competitor2: 74, score1: 22, score2: 14, turn: 5}
- {board: 25, competitor1: 107, competitor2: 15, score1: 19, score2: 15, turn: 5}
- {board: 26, competitor1: 63, competitor2: 13, score1: 8, score2: 15, turn: 5}
- {board: 27, competitor1: 16, competitor2: 52, score1: 16, score2: 24, turn: 5}
- {board: 28, competitor1: 78, competitor2: 25, score1: 14, score2: 25, turn: 5}
- {board: 29, competitor1: 59, competitor2: 30, score1: 5, score2: 25, turn: 5}
- {board: 30, competitor1: 24, competitor2: 1, score1: 14, score2: 24, turn: 5}
- {board: 31, competitor1: 89, competitor2: 75, score1: 19, score2: 15, turn: 5}
- {board: 32, competitor1: 34, competitor2: 64, score1: 25, score2: 6, turn: 5}
- {board: 33, competitor1: 29, competitor2: 44, score1: 9, score2: 14, turn: 5}
- {board: 34, competitor1: 42, competitor2: 58, score1: 12, score2: 20, turn: 5}
- {board: 35, competitor1: 17, competitor2: 109, score1: 25, score2: 7, turn: 5}
- {board: 36, competitor1: 31, competitor2: 11, score1: 25, score2: 0, turn: 5}
- {board: 37, competitor1: 61, competitor2: 14, score1: 25, score2: 1, turn: 5}
- {board: 38, competitor1: 101, competitor2: 8, score1: 5, score2: 19, turn: 5}
- {board: 39, competitor1: 71, competitor2: 60, score1: 25, score2: 7, turn: 5}
- {board: 40, competitor1: 105, competitor2: 98, score1: 6, score2: 25, turn: 5}
- {board: 41, competitor1: 3, competitor2: 51, score1: 25, score2: 0, turn: 5}
- {board: 42, competitor1: 108, competitor2: 68, score1: 25, score2: 7, turn: 5}
- {board: 43, competitor1: 26, competitor2: 92, score1: 25, score2: 12, turn: 5}
- {board: 44, competitor1: 21, competitor2: 28, score1: 25, score2: 10, turn: 5}
- {board: 45, competitor1: 55, competitor2: 102, score1: 25, score2: 0, turn: 5}
- {board: 46, competitor1: 54, competitor2: 82, score1: 17, score2: 7, turn: 5}
- {board: 47, competitor1: 6, competitor2: 91, score1: 25, score2: 5, turn: 5}
- {board: 48, competitor1: 69, competitor2: 70, score1: 19, score2: 20, turn: 5}
- {board: 49, competitor1: 57, competitor2: 77, score1: 6, score2: 24, turn: 5}
- {board: 50, competitor1: 48, competitor2: 79, score1: 25, score2: 1, turn: 5}
- {board: 51, competitor1: 32, competitor2: 72, score1: 18, score2: 19, turn: 5}
- {board: 52, competitor1: 88, competitor2: 81, score1: 0, score2: 25, turn: 5}
- {board: 53, competitor1: 47, competitor2: 80, score1: 11, score2: 12, turn: 5}
- {board: 54, competitor1: 95, competitor2: 96, score1: 25, score2: 0, turn: 5}
- {board: 55, competitor1: 103, competitor2: 85, score1: 8, score2: 14, turn: 5}
- {board: 1, competitor1: 23, competitor2: 66, score1: 24, score2: 8, turn: 6}
- {board: 2, competitor1: 90, competitor2: 37, score1: 10, score2: 23, turn: 6}
- {board: 3, competitor1: 22, competitor2: 56, score1: 21, score2: 13, turn: 6}
- {board: 4, competitor1: 33, competitor2: 110, score1: 22, score2: 10, turn: 6}
- {board: 5, competitor1: 7, competitor2: 67, score1: 23, score2: 22, turn: 6}
- {board: 6, competitor1: 38, competitor2: 45, score1: 20, score2: 25, turn: 6}
- {board: 7, competitor1: 5, competitor2: 46, score1: 22, score2: 9, turn: 6}
- {board: 8, competitor1: 27, competitor2: 35, score1: 25, score2: 17, turn: 6}
- {board: 9, competitor1: 53, competitor2: 84, score1: 21, score2: 8, turn: 6}
- {board: 10, competitor1: 39, competitor2: 83, score1: 11, score2: 18, turn: 6}
- {board: 11, competitor1: 9, competitor2: 30, score1: 8, score2: 25, turn: 6}
- {board: 12, competitor1: 40, competitor2: 97, score1: 6, score2: 20, turn: 6}
- {board: 13, competitor1: 2, competitor2: 93, score1: 25, score2: 0, turn: 6}
- {board: 14, competitor1: 4, competitor2: 31, score1: 25, score2: 5, turn: 6}
- {board: 15, competitor1: 104, competitor2: 13, score1: 25, score2: 9, turn: 6}
- {board: 16, competitor1: 106, competitor2: 107, score1: 10, score2: 15, turn: 6}
- {board: 17, competitor1: 12, competitor2: 89, score1: 25, score2: 4, turn: 6}
- {board: 18, competitor1: 20, competitor2: 58, score1: 24, score2: 19, turn: 6}
- {board: 19, competitor1: 41, competitor2: 65, score1: 24, score2: 9, turn: 6}
- {board: 20, competitor1: 94, competitor2: 99, score1: 25, score2: 6, turn: 6}
- {board: 21, competitor1: 43, competitor2: 61, score1: 25, score2: 6, turn: 6}
- {board: 22, competitor1: 87, competitor2: 25, score1: 25, score2: 6, turn: 6}
- {board: 23, competitor1: 36, competitor2: 52, score1: 25, score2: 16, turn: 6}
- {board: 24, competitor1: 62, competitor2: 10, score1: 13, score2: 21, turn: 6}
- {board: 25, competitor1: 73, competitor2: 44, score1: 24, score2: 20, turn: 6}
- {board: 26, competitor1: 34, competitor2: 1, score1: 25, score2: 4, turn: 6}
- {board: 27, competitor1: 76, competitor2: 17, score1: 4, score2: 25, turn: 6}
- {board: 28, competitor1: 71, competitor2: 18, score1: 0, score2: 25, turn: 6}
- {board: 29, competitor1: 63, competitor2: 48, score1: 0, score2: 25, turn: 6}
- {board: 30, competitor1: 50, competitor2: 8, score1: 18, score2: 5, turn: 6}
- {board: 31, competitor1: 16, competitor2: 86, score1: 12, score2: 14, turn: 6}
- {board: 32, competitor1: 42, competitor2: 108, score1: 25, score2: 10, turn: 6}
- {board: 33, competitor1: 24, competitor2: 21, score1: 13, score2: 25, turn: 6}
- {board: 34, competitor1: 14, competitor2: 72, score1: 13, score2: 17, turn: 6}
- {board: 35, competitor1: 29, competitor2: 75, score1: 25, score2: 8, turn: 6}
- {board: 36, competitor1: 19, competitor2: 3, score1: 25, score2: 4, turn: 6}
- {board: 37, competitor1: 74, competitor2: 11, score1: 25, score2: 7, turn: 6}
- {board: 38, competitor1: 49, competitor2: 26, score1: 25, score2: 4, turn: 6}
- {board: 39, competitor1: 59, competitor2: 98, score1: 4, score2: 25, turn: 6}
- {board: 40, competitor1: 78, competitor2: 109, score1: 25, score2: 0, turn: 6}
- {board: 41, competitor1: 100, competitor2: 54, score1: 17, score2: 15, turn: 6}
- {board: 42, competitor1: 15, competitor2: 77, score1: 8, score2: 10, turn: 6}
- {board: 43, competitor1: 55, competitor2: 70, score1: 25, score2: 0, turn: 6}
- {board: 44, competitor1: 6, competitor2: 64, score1: 1, score2: 25, turn: 6}
- {board: 45, competitor1: 101, competitor2: 81, score1: 8, score2: 24, turn: 6}
- {board: 46, competitor1: 69, competitor2: 92, score1: 18, score2: 8, turn: 6}
- {board: 47, competitor1: 57, competitor2: 95, score1: 25, score2: 0, turn: 6}
- {board: 48, competitor1: 105, competitor2: 91, score1: 20, score2: 16, turn: 6}
- {board: 49, competitor1: 82, competitor2: 85, score1: 13, score2: 25, turn: 6}
- {board: 50, competitor1: 32, competitor2: 28, score1: 24, score2: 18, turn: 6}
- {board: 51, competitor1: 60, competitor2: 68, score1: 25, score2: 0, turn: 6}
- {board: 52, competitor1: 51, competitor2: 79, score1: 4, score2: 25, turn: 6}
- {board: 53, competitor1: 102, competitor2: 88, score1: 9, score2: 2, turn: 6}
- {board: 54, competitor1: 80, competitor2: 103, score1: 11, score2: 4, turn: 6}
- {board: 55, competitor1: 47, competitor2: 96, score1: 25, score2: 0, turn: 6}
- {board: 1, competitor1: 33, competitor2: 45, score1: 25, score2: 9, turn: 7}
- {board: 2, competitor1: 53, competitor2: 5, score1: 25, score2: 1, turn: 7}
- {board: 3, competitor1: 27, competitor2: 23, score1: 0, score2: 25, turn: 7}
- {board: 4, competitor1: 22, competitor2: 7, score1: 25, score2: 12, turn: 7}
- {board: 5, competitor1: 90, competitor2: 97, score1: 19, score2: 12, turn: 7}
- {board: 6, competitor1: 37, competitor2: 35, score1: 14, score2: 25, turn: 7}
- {board: 7, competitor1: 66, competitor2: 30, score1: 25, score2: 0, turn: 7}
- {board: 8, competitor1: 110, competitor2: 36, score1: 9, score2: 10, turn: 7}
- {board: 9, competitor1: 56, competitor2: 12, score1: 25, score2: 12, turn: 7}
- {board: 10, competitor1: 84, competitor2: 34, score1: 2, score2: 25, turn: 7}
- {board: 11, competitor1: 83, competitor2: 73, score1: 25, score2: 7, turn: 7}
- {board: 12, competitor1: 20, competitor2: 17, score1: 0, score2: 25, turn: 7}
- {board: 13, competitor1: 104, competitor2: 40, score1: 16, score2: 16, turn: 7}
- {board: 14, competitor1: 46, competitor2: 43, score1: 0, score2: 25, turn: 7}
- {board: 15, competitor1: 2, competitor2: 87, score1: 25, score2: 2, turn: 7}
- {board: 16, competitor1: 38, competitor2: 41, score1: 18, score2: 13, turn: 7}
- {board: 17, competitor1: 94, competitor2: 4, score1: 25, score2: 6, turn: 7}
- {board: 18, competitor1: 107, competitor2: 39, score1: 9, score2: 20, turn: 7}
- {board: 19, competitor1: 67, competitor2: 10, score1: 20, score2: 8, turn: 7}
- {board: 20, competitor1: 50, competitor2: 9, score1: 0, score2: 25, turn: 7}
- {board: 21, competitor1: 62, competitor2: 25, score1: 15, score2: 15, turn: 7}
- {board: 22, competitor1: 106, competitor2: 55, score1: 15, score2: 20, turn: 7}
- {board: 23, competitor1: 58, competitor2: 72, score1: 11, score2: 6, turn: 7}
- {board: 24, competitor1: 18, competitor2: 89, score1: 25, score2: 0, turn: 7}
- {board: 25, competitor1: 65, competitor2: 48, score1: 25, score2: 6, turn: 7}
- {board: 26, competitor1: 52, competitor2: 29, score1: 3, score2: 25, turn: 7}
- {board: 27, competitor1: 13, competitor2: 100, score1: 25, score2: 7, turn: 7}
- {board: 28, competitor1: 93, competitor2: 74, score1: 24, score2: 6, turn: 7}
- {board: 29, competitor1: 99, competitor2: 78, score1: 25, score2: 8, turn: 7}
- {board: 30, competitor1: 49, competitor2: 64, score1: 18, score2: 8, turn: 7}
- {board: 31, competitor1: 31, competitor2: 21, score1: 12, score2: 20, turn: 7}
- {board: 32, competitor1: 76, competitor2: 86, score1: 25, score2: 6, turn: 7}
- {board: 33, competitor1: 77, competitor2: 8, score1: 5, score2: 23, turn: 7}
- {board: 34, competitor1: 61, competitor2: 98, score1: 25, score2: 0, turn: 7}
- {board: 35, competitor1: 42, competitor2: 1, score1: 25, score2: 2, turn: 7}
- {board: 36, competitor1: 81, competitor2: 71, score1: 9, score2: 23, turn: 7}
- {board: 37, competitor1: 44, competitor2: 19, score1: 13, score2: 25, turn: 7}
- {board: 38, competitor1: 63, competitor2: 109, score1: 19, score2: 11, turn: 7}
- {board: 39, competitor1: 24, competitor2: 11, score1: 17, score2: 20, turn: 7}
- {board: 40, competitor1: 16, competitor2: 57, score1: 25, score2: 0, turn: 7}
- {board: 41, competitor1: 6, competitor2: 79, score1: 25, score2: 13, turn: 7}
- {board: 42, competitor1: 59, competitor2: 54, score1: 10, score2: 18, turn: 7}
- {board: 43, competitor1: 108, competitor2: 60, score1: 3, score2: 25, turn: 7}
- {board: 44, competitor1: 14, competitor2: 3, score1: 10, score2: 13, turn: 7}
- {board: 45, competitor1: 105, competitor2: 70, score1: 20, score2: 11, turn: 7}
- {board: 46, competitor1: 15, competitor2: 102, score1: 25, score2: 5, turn: 7}
- {board: 47, competitor1: 32, competitor2: 80, score1: 25, score2: 0, turn: 7}
- {board: 48, competitor1: 26, competitor2: 85, score1: 25, score2: 7, turn: 7}
- {board: 49, competitor1: 75, competitor2: 69, score1: 14, score2: 21, turn: 7}
- {board: 50, competitor1: 51, competitor2: 47, score1: 19, score2: 15, turn: 7}
- {board: 51, competitor1: 82, competitor2: 95, score1: 18, score2: 4, turn: 7}
- {board: 52, competitor1: 101, competitor2: 28, score1: 25, score2: 7, turn: 7}
- {board: 53, competitor1: 92, competitor2: 68, score1: 25, score2: 0, turn: 7}
- {board: 54, competitor1: 103, competitor2: 96, score1: 25, score2: 0, turn: 7}
- {board: 55, competitor1: 88, competitor2: 91, score1: 16, score2: 15, turn: 7}
- {board: 1, competitor1: 53, competitor2: 37, score1: 3, score2: 19, turn: 8}
- {board: 2, competitor1: 22, competitor2: 33, score1: 18, score2: 21, turn: 8}
- {board: 3, competitor1: 66, competitor2: 45, score1: 25, score2: 1, turn: 8}
- {board: 4, competitor1: 5, competitor2: 83, score1: 5, score2: 25, turn: 8}
- {board: 5, competitor1: 94, competitor2: 34, score1: 9, score2: 25, turn: 8}
- {board: 6, competitor1: 56, competitor2: 67, score1: 25, score2: 11, turn: 8}
- {board: 7, competitor1: 7, competitor2: 43, score1: 11, score2: 22, turn: 8}
- {board: 8, competitor1: 39, competitor2: 40, score1: 23, score2: 17, turn: 8}
- {board: 9, competitor1: 23, competitor2: 35, score1: 19, score2: 24, turn: 8}
- {board: 10, competitor1: 27, competitor2: 90, score1: 15, score2: 7, turn: 8}
- {board: 11, competitor1: 110, competitor2: 12, score1: 25, score2: 9, turn: 8}
- {board: 12, competitor1: 2, competitor2: 38, score1: 13, score2: 18, turn: 8}
- {board: 13, competitor1: 84, competitor2: 58, score1: 13, score2: 5, turn: 8}
- {board: 14, competitor1: 36, competitor2: 17, score1: 15, score2: 25, turn: 8}
- {board: 15, competitor1: 4, competitor2: 19, score1: 18, score2: 21, turn: 8}
- {board: 16, competitor1: 97, competitor2: 104, score1: 25, score2: 0, turn: 8}
- {board: 17, competitor1: 30, competitor2: 65, score1: 10, score2: 25, turn: 8}
- {board: 18, competitor1: 46, competitor2: 13, score1: 24, score2: 7, turn: 8}
- {board: 19, competitor1: 9, competitor2: 107, score1: 25, score2: 11, turn: 8}
- {board: 20, competitor1: 20, competitor2: 42, score1: 2, score2: 25, turn: 8}
- {board: 21, competitor1: 41, competitor2: 99, score1: 25, score2: 9, turn: 8}
- {board: 22, competitor1: 76, competitor2: 93, score1: 25, score2: 4, turn: 8}
- {board: 23, competitor1: 21, competitor2: 62, score1: 0, score2: 25, turn: 8}
- {board: 24, competitor1: 73, competitor2: 29, score1: 19, score2: 18, turn: 8}
- {board: 25, competitor1: 87, competitor2: 49, score1: 25, score2: 12, turn: 8}
- {board: 26, competitor1: 18, competitor2: 55, score1: 25, score2: 8, turn: 8}
- {board: 27, competitor1: 10, competitor2: 61, score1: 13, score2: 24, turn: 8}
- {board: 28, competitor1: 52, competitor2: 69, score1: 25, score2: 6, turn: 8}
- {board: 29, competitor1: 50, competitor2: 71, score1: 11, score2: 7, turn: 8}
- {board: 30, competitor1: 106, competitor2: 15, score1: 25, score2: 0, turn: 8}
- {board: 31, competitor1: 64, competitor2: 11, score1: 18, score2: 12, turn: 8}
- {board: 32, competitor1: 8, competitor2: 25, score1: 25, score2: 7, turn: 8}
- {board: 33, competitor1: 63, competitor2: 16, score1: 18, score2: 20, turn: 8}
- {board: 34, competitor1: 44, competitor2: 86, score1: 17, score2: 15, turn: 8}
- {board: 35, competitor1: 31, competitor2: 105, score1: 15, score2: 21, turn: 8}
- {board: 36, competitor1: 78, competitor2: 100, score1: 16, score2: 9, turn: 8}
- {board: 37, competitor1: 48, competitor2: 26, score1: 13, score2: 11, turn: 8}
- {board: 38, competitor1: 6, competitor2: 32, score1: 25, score2: 12, turn: 8}
- {board: 39, competitor1: 89, competitor2: 77, score1: 6, score2: 10, turn: 8}
- {board: 40, competitor1: 74, competitor2: 3, score1: 25, score2: 0, turn: 8}
- {board: 41, competitor1: 72, competitor2: 98, score1: 10, score2: 23, turn: 8}
- {board: 42, competitor1: 101, competitor2: 24, score1: 15, score2: 20, turn: 8}
- {board: 43, competitor1: 1, competitor2: 54, score1: 20, score2: 11, turn: 8}
- {board: 44, competitor1: 60, competitor2: 81, score1: 15, score2: 24, turn: 8}
- {board: 45, competitor1: 14, competitor2: 92, score1: 12, score2: 25, turn: 8}
- {board: 46, competitor1: 75, competitor2: 85, score1: 25, score2: 0, turn: 8}
- {board: 47, competitor1: 59, competitor2: 51, score1: 22, score2: 15, turn: 8}
- {board: 48, competitor1: 108, competitor2: 79, score1: 25, score2: 7, turn: 8}
- {board: 49, competitor1: 70, competitor2: 88, score1: 2, score2: 8, turn: 8}
- {board: 50, competitor1: 109, competitor2: 82, score1: 25, score2: 0, turn: 8}
- {board: 51, competitor1: 102, competitor2: 57, score1: 0, score2: 25, turn: 8}
- {board: 52, competitor1: 80, competitor2: 28, score1: 12, score2: 18, turn: 8}
- {board: 53, competitor1: 91, competitor2: 96, score1: 25, score2: 0, turn: 8}
- {board: 54, competitor1: 47, competitor2: 103, score1: 10, score2: 4, turn: 8}
- {board: 55, competitor1: 68, competitor2: 95, score1: 23, score2: 2, turn: 8}
- {board: 1, competitor1: 83, competitor2: 17, score1: 22, score2: 11, turn: 9}
- {board: 2, competitor1: 97, competitor2: 36, score1: 25, score2: 11, turn: 9}
- {board: 3, competitor1: 90, competitor2: 39, score1: 21, score2: 14, turn: 9}
- {board: 4, competitor1: 34, competitor2: 56, score1: 25, score2: 6, turn: 9}
- {board: 5, competitor1: 53, competitor2: 43, score1: 5, score2: 25, turn: 9}
- {board: 6, competitor1: 2, competitor2: 65, score1: 21, score2: 11, turn: 9}
- {board: 7, competitor1: 110, competitor2: 41, score1: 24, score2: 11, turn: 9}
- {board: 8, competitor1: 23, competitor2: 22, score1: 9, score2: 15, turn: 9}
- {board: 9, competitor1: 27, competitor2: 37, score1: 13, score2: 15, turn: 9}
- {board: 10, competitor1: 66, competitor2: 38, score1: 16, score2: 19, turn: 9}
- {board: 11, competitor1: 35, competitor2: 33, score1: 24, score2: 22, turn: 9}
- {board: 12, competitor1: 94, competitor2: 67, score1: 25, score2: 0, turn: 9}
- {board: 13, competitor1: 45, competitor2: 87, score1: 25, score2: 11, turn: 9}
- {board: 14, competitor1: 7, competitor2: 76, score1: 25, score2: 8, turn: 9}
- {board: 15, competitor1: 5, competitor2: 61, score1: 0, score2: 25, turn: 9}
- {board: 16, competitor1: 84, competitor2: 42, score1: 1, score2: 25, turn: 9}
- {board: 17, competitor1: 46, competitor2: 18, score1: 24, score2: 12, turn: 9}
- {board: 18, competitor1: 62, competitor2: 8, score1: 25, score2: 0, turn: 9}
- {board: 19, competitor1: 19, competitor2: 104, score1: 18, score2: 16, turn: 9}
- {board: 20, competitor1: 30, competitor2: 48, score1: 25, score2: 15, turn: 9}
- {board: 21, competitor1: 73, competitor2: 9, score1: 13, score2: 13, turn: 9}
- {board: 22, competitor1: 40, competitor2: 50, score1: 25, score2: 6, turn: 9}
- {board: 23, competitor1: 4, competitor2: 106, score1: 16, score2: 10, turn: 9}
- {board: 24, competitor1: 20, competitor2: 6, score1: 8, score2: 24, turn: 9}
- {board: 25, competitor1: 10, competitor2: 1, score1: 16, score2: 17, turn: 9}
- {board: 26, competitor1: 93, competitor2: 16, score1: 12, score2: 25, turn: 9}
- {board: 27, competitor1: 12, competitor2: 74, score1: 12, score2: 6, turn: 9}
- {board: 28, competitor1: 99, competitor2: 13, score1: 25, score2: 4, turn: 9}
- {board: 29, competitor1: 49, competitor2: 105, score1: 25, score2: 11, turn: 9}
- {board: 30, competitor1: 58, competitor2: 55, score1: 20, score2: 10, turn: 9}
- {board: 31, competitor1: 81, competitor2: 63, score1: 1, score2: 25, turn: 9}
- {board: 32, competitor1: 107, competitor2: 78, score1: 25, score2: 8, turn: 9}
- {board: 33, competitor1: 71, competitor2: 86, score1: 4, score2: 25, turn: 9}
- {board: 34, competitor1: 77, competitor2: 25, score1: 7, score2: 25, turn: 9}
- {board: 35, competitor1: 52, competitor2: 21, score1: 20, score2: 9, turn: 9}
- {board: 36, competitor1: 29, competitor2: 98, score1: 24, score2: 17, turn: 9}
- {board: 37, competitor1: 108, competitor2: 32, score1: 5, score2: 25, turn: 9}
- {board: 38, competitor1: 44, competitor2: 64, score1: 11, score2: 12, turn: 9}
- {board: 39, competitor1: 15, competitor2: 72, score1: 10, score2: 13, turn: 9}
- {board: 40, competitor1: 24, competitor2: 60, score1: 9, score2: 25, turn: 9}
- {board: 41, competitor1: 89, competitor2: 109, score1: 10, score2: 25, turn: 9}
- {board: 42, competitor1: 69, competitor2: 11, score1: 17, score2: 19, turn: 9}
- {board: 43, competitor1: 59, competitor2: 75, score1: 25, score2: 20, turn: 9}
- {board: 44, competitor1: 31, competitor2: 54, score1: 25, score2: 0, turn: 9}
- {board: 45, competitor1: 100, competitor2: 57, score1: 20, score2: 14, turn: 9}
- {board: 46, competitor1: 26, competitor2: 88, score1: 25, score2: 0, turn: 9}
- {board: 47, competitor1: 3, competitor2: 92, score1: 16, score2: 19, turn: 9}
- {board: 48, competitor1: 101, competitor2: 14, score1: 23, score2: 22, turn: 9}
- {board: 49, competitor1: 28, competitor2: 85, score1: 24, score2: 13, turn: 9}
- {board: 50, competitor1: 70, competitor2: 80, score1: 5, score2: 17, turn: 9}
- {board: 51, competitor1: 47, competitor2: 95, score1: 21, score2: 0, turn: 9}
- {board: 52, competitor1: 102, competitor2: 91, score1: 21, score2: 11, turn: 9}
- {board: 53, competitor1: 51, competitor2: 82, score1: 8, score2: 18, turn: 9}
- {board: 54, competitor1: 79, competitor2: 68, score1: 15, score2: 9, turn: 9}
- {board: 55, competitor1: 103, competitor2: null, score1: 25, score2: 0, turn: 9}
- {board: 1, competitor1: 34, competitor2: 43, score1: 8, score2: 22, turn: 10}
- {board: 2, competitor1: 110, competitor2: 46, score1: 25, score2: 2, turn: 10}
- {board: 3, competitor1: 97, competitor2: 53, score1: 25, score2: 10, turn: 10}
- {board: 4, competitor1: 90, competitor2: 66, score1: 6, score2: 25, turn: 10}
- {board: 5, competitor1: 27, competitor2: 83, score1: 25, score2: 10, turn: 10}
- {board: 6, competitor1: 94, competitor2: 42, score1: 13, score2: 17, turn: 10}
- {board: 7, competitor1: 35, competitor2: 22, score1: 18, score2: 17, turn: 10}
- {board: 8, competitor1: 37, competitor2: 33, score1: 5, score2: 25, turn: 10}
- {board: 9, competitor1: 45, competitor2: 61, score1: 5, score2: 25, turn: 10}
- {board: 10, competitor1: 56, competitor2: 17, score1: 24, score2: 13, turn: 10}
- {board: 11, competitor1: 38, competitor2: 23, score1: 3, score2: 25, turn: 10}
- {board: 12, competitor1: 19, competitor2: 39, score1: 16, score2: 25, turn: 10}
- {board: 13, competitor1: 73, competitor2: 62, score1: 25, score2: 10, turn: 10}
- {board: 14, competitor1: 9, competitor2: 40, score1: 13, score2: 24, turn: 10}
- {board: 15, competitor1: 84, competitor2: 99, score1: 13, score2: 17, turn: 10}
- {board: 16, competitor1: 2, competitor2: 7, score1: 16, score2: 13, turn: 10}
- {board: 17, competitor1: 36, competitor2: 107, score1: 20, score2: 24, turn: 10}
- {board: 18, competitor1: 5, competitor2: 52, score1: 25, score2: 9, turn: 10}
- {board: 19, competitor1: 4, competitor2: 6, score1: 25, score2: 0, turn: 10}
- {board: 20, competitor1: 87, competitor2: 12, score1: 16, score2: 16, turn: 10}
- {board: 21, competitor1: 67, competitor2: 16, score1: 25, score2: 0, turn: 10}
- {board: 22, competitor1: 41, competitor2: 58, score1: 25, score2: 4, turn: 10}
- {board: 23, competitor1: 30, competitor2: 29, score1: 20, score2: 1, turn: 10}
- {board: 24, competitor1: 65, competitor2: 1, score1: 25, score2: 1, turn: 10}
- {board: 25, competitor1: 8, competitor2: 20, score1: 24, score2: 17, turn: 10}
- {board: 26, competitor1: 104, competitor2: 50, score1: 25, score2: 0, turn: 10}
- {board: 27, competitor1: 64, competitor2: 18, score1: 21, score2: 22, turn: 10}
- {board: 28, competitor1: 76, competitor2: 49, score1: 18, score2: 8, turn: 10}
- {board: 29, competitor1: 25, competitor2: 10, score1: 0, score2: 25, turn: 10}
- {board: 30, competitor1: 78, competitor2: 32, score1: 23, score2: 18, turn: 10}
- {board: 31, competitor1: 44, competitor2: 55, score1: 22, score2: 18, turn: 10}
- {board: 32, competitor1: 93, competitor2: 72, score1: 21, score2: 9, turn: 10}
- {board: 33, competitor1: 106, competitor2: 105, score1: 20, score2: 13, turn: 10}
- {board: 34, competitor1: 48, competitor2: 59, score1: 25, score2: 4, turn: 10}
- {board: 35, competitor1: 63, competitor2: 100, score1: 21, score2: 18, turn: 10}
- {board: 36, competitor1: 13, competitor2: 109, score1: 23, score2: 7, turn: 10}
- {board: 37, competitor1: 31, competitor2: 60, score1: 20, score2: 17, turn: 10}
- {board: 38, competitor1: 86, competitor2: 11, score1: 25, score2: 1, turn: 10}
- {board: 39, competitor1: 101, competitor2: 69, score1: 2, score2: 25, turn: 10}
- {board: 40, competitor1: 26, competitor2: 77, score1: 9, score2: 25, turn: 10}
- {board: 41, competitor1: 81, competitor2: 24, score1: 14, score2: 14, turn: 10}
- {board: 42, competitor1: 98, competitor2: 92, score1: 22, score2: 13, turn: 10}
- {board: 43, competitor1: 74, competitor2: 21, score1: 10, score2: 18, turn: 10}
- {board: 44, competitor1: 89, competitor2: 80, score1: 25, score2: 6, turn: 10}
- {board: 45, competitor1: 71, competitor2: 108, score1: 25, score2: 5, turn: 10}
- {board: 46, competitor1: 28, competitor2: 57, score1: 14, score2: 15, turn: 10}
- {board: 47, competitor1: 15, competitor2: 88, score1: 25, score2: 4, turn: 10}
- {board: 48, competitor1: 75, competitor2: 82, score1: 25, score2: 2, turn: 10}
- {board: 49, competitor1: 3, competitor2: 79, score1: 25, score2: 10, turn: 10}
- {board: 50, competitor1: 54, competitor2: 102, score1: 25, score2: 0, turn: 10}
- {board: 51, competitor1: 70, competitor2: 68, score1: 22, score2: 11, turn: 10}
- {board: 52, competitor1: 47, competitor2: 14, score1: 8, score2: 16, turn: 10}
- {board: 53, competitor1: 51, competitor2: 103, score1: 13, score2: 7, turn: 10}
- {board: 54, competitor1: 85, competitor2: 91, score1: 9, score2: 13, turn: 10}
- {board: 55, competitor1: 95, competitor2: null, score1: 25, score2: 0, turn: 10}
- {board: 1, competitor1: 38, competitor2: 110, score1: 5, score2: 25, turn: 11}
- {board: 2, competitor1: 83, competitor2: 61, score1: 25, score2: 4, turn: 11}
- {board: 3, competitor1: 34, competitor2: 2, score1: 22, score2: 16, turn: 11}
- {board: 4, competitor1: 39, competitor2: 73, score1: 25, score2: 5, turn: 11}
- {board: 5, competitor1: 46, competitor2: 30, score1: 10, score2: 23, turn: 11}
- {board: 6, competitor1: 33, competitor2: 23, score1: 11, score2: 20, turn: 11}
- {board: 7, competitor1: 42, competitor2: 90, score1: 18, score2: 20, turn: 11}
- {board: 8, competitor1: 94, competitor2: 76, score1: 20, score2: 19, turn: 11}
- {board: 9, competitor1: 7, competitor2: 19, score1: 20, score2: 19, turn: 11}
- {board: 10, competitor1: 37, competitor2: 22, score1: 23, score2: 7, turn: 11}
- {board: 11, competitor1: 27, competitor2: 43, score1: 21, score2: 25, turn: 11}
- {board: 12, competitor1: 4, competitor2: 107, score1: 22, score2: 6, turn: 11}
- {board: 13, competitor1: 35, competitor2: 97, score1: 25, score2: 4, turn: 11}
- {board: 14, competitor1: 66, competitor2: 56, score1: 25, score2: 0, turn: 11}
- {board: 15, competitor1: 45, competitor2: 67, score1: 15, score2: 21, turn: 11}
- {board: 16, competitor1: 65, competitor2: 99, score1: 17, score2: 7, turn: 11}
- {board: 17, competitor1: 40, competitor2: 53, score1: 15, score2: 22, turn: 11}
- {board: 18, competitor1: 41, competitor2: 18, score1: 25, score2: 2, turn: 11}
- {board: 19, competitor1: 5, competitor2: 17, score1: 5, score2: 25, turn: 11}
- {board: 20, competitor1: 36, competitor2: 93, score1: 25, score2: 0, turn: 11}
- {board: 21, competitor1: 104, competitor2: 12, score1: 0, score2: 25, turn: 11}
- {board: 22, competitor1: 84, competitor2: 48, score1: 22, score2: 16, turn: 11}
- {board: 23, competitor1: 9, competitor2: 8, score1: 25, score2: 0, turn: 11}
- {board: 24, competitor1: 64, competitor2: 78, score1: 24, score2: 15, turn: 11}
- {board: 25, competitor1: 16, competitor2: 6, score1: 20, score2: 17, turn: 11}
- {board: 26, competitor1: 62, competitor2: 87, score1: 22, score2: 6, turn: 11}
- {board: 27, competitor1: 49, competitor2: 63, score1: 25, score2: 10, turn: 11}
- {board: 28, competitor1: 10, competitor2: 86, score1: 20, score2: 14, turn: 11}
- {board: 29, competitor1: 29, competitor2: 13, score1: 11, score2: 25, turn: 11}
- {board: 30, competitor1: 52, competitor2: 31, score1: 4, score2: 25, turn: 11}
- {board: 31, competitor1: 106, competitor2: 58, score1: 25, score2: 3, turn: 11}
- {board: 32, competitor1: 44, competitor2: 98, score1: 11, score2: 17, turn: 11}
- {board: 33, competitor1: 20, competitor2: 81, score1: 13, score2: 24, turn: 11}
- {board: 34, competitor1: 69, competitor2: 109, score1: 25, score2: 5, turn: 11}
- {board: 35, competitor1: 77, competitor2: 50, score1: 13, score2: 25, turn: 11}
- {board: 36, competitor1: 74, competitor2: 75, score1: 19, score2: 9, turn: 11}
- {board: 37, competitor1: 25, competitor2: 71, score1: 25, score2: 9, turn: 11}
- {board: 38, competitor1: 89, competitor2: 3, score1: 4, score2: 25, turn: 11}
- {board: 39, competitor1: 26, competitor2: 100, score1: 22, score2: 15, turn: 11}
- {board: 40, competitor1: 1, competitor2: 21, score1: 11, score2: 22, turn: 11}
- {board: 41, competitor1: 59, competitor2: 32, score1: 9, score2: 20, turn: 11}
- {board: 42, competitor1: 28, competitor2: 79, score1: 25, score2: 8, turn: 11}
- {board: 43, competitor1: 15, competitor2: 57, score1: 23, score2: 11, turn: 11}
- {board: 44, competitor1: 55, competitor2: 11, score1: 25, score2: 0, turn: 11}
- {board: 45, competitor1: 82, competitor2: 70, score1: 1, score2: 24, turn: 11}
- {board: 46, competitor1: 60, competitor2: 101, score1: 10, score2: 10, turn: 11}
- {board: 47, competitor1: 105, competitor2: 54, score1: 11, score2: 4, turn: 11}
- {board: 48, competitor1: 80, competitor2: 91, score1: 7, score2: 9, turn: 11}
- {board: 49, competitor1: 72, competitor2: 92, score1: 17, score2: 10, turn: 11}
- {board: 50, competitor1: 24, competitor2: 14, score1: 25, score2: 9, turn: 11}
- {board: 51, competitor1: 108, competitor2: 88, score1: 22, score2: 5, turn: 11}
- {board: 52, competitor1: 47, competitor2: 85, score1: 24, score2: 4, turn: 11}
- {board: 53, competitor1: 68, competitor2: null, score1: 25, score2: 0, turn: 11}
- {board: 54, competitor1: 102, competitor2: 51, score1: 3, score2: 25, turn: 11}
- {board: 55, competitor1: 95, competitor2: 103, score1: 5, score2: 16, turn: 11}
modified: 2013-12-22 11:03:36
prealarm: 5
prized: true
rankedturn: 11
championship: 1
